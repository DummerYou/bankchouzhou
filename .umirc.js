// ref: https://umijs.org/config/
export default {
  // base: '/',//子目录地址
  history: 'hash',
  treeShaking: true,
  routes: [
    {
      path: '/',
      component: '../layouts/index',
      routes: [{ path: '/', component: '../pages/index'},
      { path: '/addItem', component: '../pages/addItem/index'},
      { path: '/detail', component: '../pages/detail/index' },
      { path: '/contrast', component: '../pages/contrast/index'},
      { path: '/newCreate', component: '../pages/newCreate'},
      //页面
      { path: '/secondCategory', component: '../components/secondCategory/index'},
      { path: '/moreClassify', component: '../components/moreClassify/index'},
      { path: '/flyBack', component: '../components/flyback/index'},
      // { path: '/home', component: '../pages/home/index' },
      // { path: '/featureCenterList', component: '../pages/featureCenterList/index' },
      // { path: '/featuresNew', component: '../pages/featuresNew/index' },
      // { path: '/BusinessMonitoringQuery', component: '../pages/BusinessMonitoringQuery/index' },
    ],
    },
  ],
  plugins: [
    // ref: https://umijs.org/plugin/umi-plugin-react.html
    [
      'umi-plugin-react',
      {
        antd: true,
        dva: true,
        dynamicImport: false,
        title: 'bankchouzhou',
        dll: false,

        routes: {
          exclude: [
            /models\//,
            /services\//,
            /model\.(t|j)sx?$/,
            /service\.(t|j)sx?$/,
            /components\//,
          ],
        },
      },
    ],
  ],
  theme: {
    // 'primary-color': '#d4343c',
  },
  proxy: {
    // '/data': {
    //     target: 'http://172.16.5.23:9090',
    //     changeOrigin: true,
    //     // pathRewrite: { '^/server': '' },
    // },
    '/urlAPI': {
      // target: 'http://10.104.1.141:9999',
      target: 'http://10.104.1.142:8081',
      // target: 'http://10.104.1.141:8887/',
      changeOrigin: true,
      pathRewrite: { '^/urlAPI': '' },
      cookieDomainRewrite: {
        '*': 'localhost',
      },
    },
  },
};
