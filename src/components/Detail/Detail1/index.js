import React, { PureComponent, Fragment } from 'react';
import styles from './index.css';
import { Input, Button, Icon, DatePicker, Table } from 'antd';
import { connect } from 'dva';
import moment from 'moment';
import { withRouter } from 'react-router';

@connect(({ feature }) => ({
  feature,
}))
class Detail1 extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      items: {},
      realTime: {},
      timeTxt:"",
      timestate:false,
    };
  }

  componentDidMount() {
    const {
      dispatch,
      location: { query },
    } = this.props;

    dispatch({
      type: 'feature/predictor',
      payload: {
        serverId: query.id,
      },
    }).then(res => {
      console.log(res);
      if (res.code == 200) {
        var data = res.data;
        console.log(data);
        if(data.isOnline){
          this.setState({
            realTime: data.realTimePredictorVo || {},
          });
          this.timerID()
        }else{
          this.setState({
            items: data.batchPredictorVo || {},
          });
        }
      }
    });
  }
  timerID = () =>{
    this.timerID = setInterval(() => {
      this.getrealTimePredictor()
    }, 30*1000);
  }
  componentWillUnmount() {
    clearInterval(this.timerID);
  }
  getrealTimePredictor = () =>{
    
    const {
      dispatch,
      location: { query },
    } = this.props;
    
    this.setState({
      timestate: true,
    });
    dispatch({
      type: 'feature/predictor',
      payload: {
        serverId: query.id,
      },
    }).then(res => {
      console.log(res);
      if (res.code == 200) {
        var data = res.data;
        if(data.isOnline){
          this.setState({
            realTime: data.realTimePredictorVo || {},
          });
        }
      }
        this.setState({
          timeTxt: moment().format('YYYY-MM-DD h:mm:ss'),
          timestate: false,
        });
        this.forceUpdate();
    });
  }
  numjia0 = j => {
    if (j < 10) {
      j = '0' + j;
    }
    return j;
  };
  setTimes = (x) =>{
    var d = moment.duration(x || 0, 'seconds');
     return (d.asDays() >= 1 ? Math.floor(d.asDays()) + '天' : '') +
        this.numjia0(d.hours()) +
        ':' +
        this.numjia0(d.minutes()) +
        ':' +
        this.numjia0(d.seconds())
  }
  render() {
    const { items,realTime,timestate,timeTxt } = this.state;
    console.log(items);

    return (
      <div className={styles.bmq}>
        {items.totalNumber?
          <div className={' borderDiv'}>
          <div className={' borderDivT'}>批量预估监控</div>
          <div className={' borderDivList'}>
            <span className={' borderltems'}>总运行次数：{items.totalNumber || 0}次</span>
            <span className={' borderltems'}>平均运行时长：{this.setTimes(items.avgTime)}</span>
            <span className={' borderltems'}>预估结果数：{items.estimateNumber || 0}次</span>
            <span className={' borderltems'}>异常记录：{items.exceptionNumber || 0}条</span>
            <a
              className={' borderltems'}
              href={items.detailUrl}
              target="_blank"
              rel="noopener noreferrer"
            >
              查看详情
            </a>
          </div>
          
        </div>:null
        }
        {realTime.total?
          <div className={' borderDiv mt10'}>
          <div className={' borderDivT'}><span>实时预估监控</span><span className={styles.mlr20}>{moment(realTime.time*1000).format('YYYY-MM-DD h:mm:ss')}</span><Icon className={styles.pur} onClick={this.getrealTimePredictor} type="reload" spin={timestate}  /></div>
          <div className={' borderDivList'}>
            <div className={styles.bList + ' ' + styles.nopleft}>
              <p className={' fs18 weight6'}>{(Number(realTime.total) || 0).toFixed(0)}</p>
              <span>总访问量</span>
            </div>
            <div className={styles.bList}>
              <p className={' fs18 weight6'}>{(Number(realTime.overtime) || 0).toFixed(0)}/{(Number(realTime.overtimeRate) || 0).toFixed(2)}%</p>
              <span>超时总数/超时比例<br />超时阈值1s</span>
            </div>
            <div className={styles.bList}>
              <p className={' fs18 weight6'}>{(Number(realTime.qps) || 0).toFixed(0)}</p>
              <span>QPS</span>
            </div>
            <div className={styles.bList}>
              <p className={' fs18 weight6'}>{(Number(realTime.tp99) || 0).toFixed(0)}ms</p>
              <span>TP99</span>
            </div>
            <div className={styles.bList}>
              <p className={' fs18 weight6'}>{(Number(realTime.accuracy) || 0).toFixed(2)}%</p>
              <span>返回码正确率</span>
            </div>
          </div>
        </div>:null
        }
      </div>
    );
  }
}


export default withRouter(Detail1);
