import React, { PureComponent, Fragment } from 'react';
import styles from './index.css';
import { Input, Button, Icon, DatePicker ,Table } from 'antd';
import { connect } from 'dva';
import moment from 'moment';
import { withRouter } from 'react-router';

@connect(({ feature }) => ({
  feature,
}))
class Detail2 extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      items:{},
      stability:{},
      effectiveness:{},
      results1:[],
      current1: 1,
      size1: 5,
      total1: 0,
      results2:[],
      current2: 1,
      size2: 5,
      total2: 0,
    };
  }

  componentDidMount() {
    // const {
    //   dispatch,
    //   location: { query },
    // } = this.props;
    // dispatch({
    //   type: 'feature/history',
    //   payload: {
    //     serverId:query.id,
    //   },
    // }).then(res => {
    //   console.log(res);
    //   if (res.code == 200) {
    //     var data = res.data;
    //     this.setState({
    //       stability: data.stability || {},
    //       effectiveness: data.effectiveness || {},
    //     });
    //   }
    // });
    this.getTable1()
    this.getTable2()
  }
  getTable1 = num => {
    const { current1, size1 } = this.state;
    const {
      dispatch,
      location: { query },
    } = this.props;
    dispatch({
      type: 'feature/stability',
      payload: {
        current: num ? num : current1,
        size:size1,
        serverId:query.id,
      },
    }).then(res => {
      console.log(res);
      if (res.code==200) {
          var data = res.data.history || {};
          this.setState({
            current1: data.page,
            size1: data.size,
            total1: data.total,
            results1: data.runRecords,
            stability:data,
          });
      }
    })
    
  }
  getTable2 =  num =>{
    const { current2, size2 } = this.state;
    const {
      dispatch,
      location: { query },
    } = this.props;
    dispatch({
      type: 'feature/effectiveness',
      payload: {
        current: num ? num : current2,
        size:size2,
        serverId:query.id,
      },
    }).then(res => {
      console.log(res);
      if (res.code==200) {
          var data = res.data.history || {};
          this.setState({
            current2: data.page,
            size2: data.size,
            total2: data.total,
            results2: data.runRecords,
            effectiveness:data,
          });
      }
    })
  }
  numjia0 = j => {
    if (j < 10) {
      j = '0' + j;
    }
    return j;
  };
  setTimes = (x) =>{
    var d = moment.duration(x || 0, 'seconds');
     return (d.asDays() >= 1 ? Math.floor(d.asDays()) + '天' : '') +
        this.numjia0(d.hours()) +
        ':' +
        this.numjia0(d.minutes()) +
        ':' +
        this.numjia0(d.seconds())
  }
  setTxtState(num){
    if(num == 0){
      return "未完成"
    }else if(num == 1){
      return "成功"
    }else if(num == 2){
      return "失败"
    }else if(num == 3){
      return "超时"
    }else{
      return "其他"
    }
  }

  render() {
    const { stability, effectiveness,current1, size1, total1,results1,current2, size2, total2,results2} = this.state;
    const columns = [
      {
        title: '开始时间',
        dataIndex: 'startTime',
        key: 'startTime',
      },
      {
        title: '结束时间',
        dataIndex: 'endTime',
        key: 'endTime',
      },
      {
        title: '当前状态',
        dataIndex: 'status',
        key: 'status',
        render: (text, record) => (
          <span>{this.setTxtState(text)}</span>
        ),
      }
    ];
    
    const data = [
      {
        key: '1',
        name: '2021-7-16 20:10:06',
        name2: '2021-7-16 20:10:06',
        age: '运行成功',
      },
    ];
    const pagination1 = {
      current: current1,
      pageSize: size1,
      total: total1,
      showQuickJumper: true,
      onChange: (date, dateString) => {
        console.log(date);
        this.setState(
          {
            current1: date,
          },
          this.getTable1(date),
        );
      },
    };
    const pagination2 = {
      current: current2,
      pageSize: size2,
      total: total2,
      showQuickJumper: true,
      onChange: (date, dateString) => {
        console.log(date);
        this.setState(
          {
            current2: date,
          },
          this.getTable2(date),
        );
      },
    };
    return (
      <div className={styles.bmq}>
        <div className={' borderDiv'}>
          <div className={' borderDivT'}>模型稳定性监控</div>
          <div className={' borderDivList'}>
            <span className={' borderltems'}>运行总次数：{stability.total || 0}次</span>
            <span className={' borderltems'}>平均运行时长：{this.setTimes(stability.avgTime)}</span>
            <span className={' borderltems'}>最高PSI：{Number(stability.max).toFixed(2) || 0}</span>
            <span className={' borderltems'}>超过阀值：{stability.overThresholdTime || 0}次</span>
          </div>
          <div className={' borderDivList mtb10'}>
            <span className={' borderltems'}>运行记录</span>
          </div>
          <div className={styles.table}>
            <Table bordered columns={columns} dataSource={results1} pagination={pagination1} size="middle"/>
          </div>
          <div className={' borderDivT'}>模型有效性和变量监控</div>
          <div className={' borderDivList'}>
            <span className={' borderltems'}>运行总次数：{effectiveness.total || 0}次</span>
            <span className={' borderltems'}>平均运行时长：{this.setTimes(effectiveness.avgTime)}</span>
            <span className={' borderltems'}>最低AUC：{Number(effectiveness.max).toFixed(2) || 0}</span>
            <span className={' borderltems'}>超过阀值：{effectiveness.overThresholdTime || 0}次</span>
          </div>
          <div className={' borderDivList mtb10'}>
            <span className={' borderltems'}>运行记录</span>
          </div>
          <div className={styles.table}>
            <Table bordered columns={columns} dataSource={results2} pagination={pagination2} size="middle"/>
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(Detail2);