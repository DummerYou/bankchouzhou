import React, { PureComponent, Fragment } from 'react';
import styles from './index.css';
import { Input, Button, Icon, DatePicker, Table, Alert } from 'antd';
import * as echarts from 'echarts';
import { connect } from 'dva';
import moment from 'moment';
import { withRouter } from 'react-router';

const { MonthPicker, RangePicker, WeekPicker } = DatePicker;

@connect(({ feature }) => ({
  feature,
}))
class Detail3 extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      items: [],
      pointsList: [],
      startDate: moment().subtract(1, 'months'),
      endDate: moment(),
      selectedRowKeys: [0],
    };
  }

  componentDidMount() {
    const {
      dispatch,
      location: {
        query
      },
    } = this.props;
    this.setDispatch1();
    this.setDispatch2();


  }
  setDispatch1 = () => {
    const {
      dispatch,
      location: {
        query
      },
    } = this.props;
    const {
      startDate,
      endDate
    } = this.state;
    dispatch({
      type: 'feature/lineChart',
      payload: {
        endDate: moment(endDate).format('YYYY-MM-DD HH:mm:ss'),
        startDate: moment(startDate).format('YYYY-MM-DD HH:mm:ss'),
        serverId: query.id,
      },
    }).then(res => {
      console.log(res);
      if (res.code == 200) {
        this.setEcharts1(res.data)
      }
    });

  }
  setDispatch2 = () => {
    const {
      dispatch,
      location: {
        query
      },
    } = this.props;
    const {
      startDate,
      endDate
    } = this.state;
    dispatch({
      type: 'feature/psiReport',
      payload: {
        serverId: query.id,
        "current": 1,
        "endDate": moment(endDate).format('YYYY-MM-DD HH:mm:ss'),
        "startDate": moment(startDate).format('YYYY-MM-DD HH:mm:ss'),
        "size": 10,
      },
    }).then(res => {
      console.log(res);
      if (res.code == 200) {
        var data2 = res.data;
        this.setState({
          items: data2.data || {},
        });
        if(data2.data[0]){
          this.setpointsList(data2.data[0])
        }
      }
    });


  }
  setEcharts1 = (data) =>{
    if(!data) return 
    var myChart1 = echarts.init(document.getElementById('forms31'));
    myChart1.clear()
    // 基于准备好的dom，初始化echarts实例
    // 绘制图表
    myChart1.setOption({
      grid: {
        top: '20px',
        left: '50px',
        right: '0',
        bottom: '50px',
    },
    tooltip: {
      trigger: 'axis',
      axisPointer: {
        type: 'shadow',
      },
      formatter: function(params){
        return params.map((v)=>{
          return v.name + "</br>" + v.marker + v.seriesName + '&nbsp;&nbsp;&nbsp;&nbsp;' +  Number(v.data).toFixed(2)
        }).join("</br>");
      },
    },
      xAxis: {
        type: 'category',
        data: data.xList,
      },
      yAxis: {
        type: 'value',
      },
      series: [
        {
          data: data.yList,
          name: "稳定性",
          type: 'line',
        },
      ],
    });
  }
  setEcharts2 = (data) =>{
    console.log(data)
    if(!data) return 
    var myChart2 = echarts.init(document.getElementById('forms32'));
    myChart2.clear()
    let option = {
      grid: {
        top: '20px',
        left: '50px',
        right: '0',
        bottom: '50px',
    },
    tooltip: {
      trigger: 'axis',
      axisPointer: {
        type: 'shadow',
      },
      formatter: function(params){
        return params.map((v)=>{
          return v.marker + v.seriesName + '&nbsp;&nbsp;&nbsp;&nbsp;' +  Number(v.data).toFixed(3)
        }).join("</br>");
      },
    },
      xAxis: {
        type: 'category',
        data: data.xlist,
      },
      yAxis: {
        type: 'value',
      },
      series: [],
    }
    for (const key in data.ylist[0]) {
      console.log(key)
      option.series.push({
        name: key,
            type: 'bar',
            data: data.ylist[0][key]
      })
    }
    myChart2.setOption(option);
  }
  onChange1 = (date, dateString) => {
    console.log(date, dateString);
    this.setState({
      startDate:date
    },()=>{
      this.setDispatch1();
    this.setDispatch2();
    });
  };
  onChange2 = (date, dateString) => {
    console.log(date, dateString);
    this.setState({
      endDate:date
    },()=>{
      this.setDispatch1();
    this.setDispatch2();
    });
  };
  /*
   * 单，多选类型时候使用
   * */

  setpointsList = data =>{
    console.log(data)
    this.setState({
      pointsList:data
    });
    this.setEcharts2(data.groupHistogram)
  }

  render() {
    const { items ,pointsList,startDate,endDate } = this.state;
    const leftcolumns = [
      {
        title: '计算时间',
        dataIndex: 'startDate',
        key: 'startDate',
      },
      {
        title: 'PSI',
        dataIndex: 'psi',
        key: 'psi',
        render: (text, record) => (
          Number(text).toFixed(2) 
        ),
      },
    ];
    const yuche = [
      {
        title: '统计项',
        dataIndex: 'statistic',
        key: 'statistic',
      },
      {
        title: '本次预估',
        dataIndex: 'thisResult',
        key: 'thisResult',
        render: (text, record) => (
          Number(text).toFixed(4)
        ),
      },
      {
        title: '基准值',
        dataIndex: 'standardResult',
        key: 'standardResult',
        render: (text, record) => (
          Number(text).toFixed(4)
        ),
      },
      {
        title: '偏差率',
        dataIndex: 'deviationRate',
        key: 'deviationRate',
      },
     
    ];
    const moxing = [
      {
        title: '组号',
        dataIndex: 'number',
        key: 'number',
      },
      {
        title: '实际分组数据占比(Ac)',
        dataIndex: 'ac',
        key: 'ac',
        render: (text, record) => (
          Number(text).toFixed(2) +"%"
        ),
      },
      {
        title: '预计分组数据占比(Ex)',
        dataIndex: 'ex',
        key: 'ex',
        render: (text, record) => (
          Number(text).toFixed(2) +"%"
        ),
      },
      {
        title: 'Ac-Ex(a)',
        dataIndex: 'a',
        key: 'a',
        render: (text, record) => (
          Number(text).toFixed(2) +"%"
        ),
      },
      {
        title: 'In(Ac/Ex)(b)',
        dataIndex: 'b',
        key: 'b',
        render: (text, record) => (
          Number(text).toFixed(4)
        ),
      },
      {
        title: 'index(a*b)',
        dataIndex: 'index',
        key: 'index',
        render: (text, record) => (
          Number(text).toFixed(4)
        ),
      },
    ];
    

    return (
      <div className={styles.bmq + " mt10"}>
        <div className={'flexLr'}>
          <div className={' borderDivT'}>模型稳定性报告</div>
          <div className={styles.right}>
            <span className={styles.mlr10}>开始时间</span>
            <DatePicker value={startDate} onChange={this.onChange1} />
            <span className={styles.mlr10}>结束时间</span>
            <DatePicker value={endDate} onChange={this.onChange2} />
          </div>
        </div>
        <div className={styles.echTop} id="forms31"></div>
        <div className={styles.content}>
          <div className={styles.contentL}>
            <Table
              bordered
              columns={leftcolumns}
              className={"radioTable"}
              rowSelection={{
                type: 'radio',
                columnWidth:"1px",
                onChange:  (selectedRowKeys, selectedRows) => {
                  console.log(selectedRowKeys);
                  this.setState({
                    selectedRowKeys: selectedRowKeys,
                  });
                },
                selectedRowKeys: this.state.selectedRowKeys,
              }}
              dataSource={items}
              pagination={false}
              onRow={(record,index )=> {
                return {
                  onClick: event => {
                    this.setpointsList(record)
                    console.log(record);
                    console.log(index);
                    this.setState({
                      selectedRowKeys: [index],
                    });
                  }, // 点击行
                  onDoubleClick: event => {},
                  onContextMenu: event => {},
                  onMouseEnter: event => {}, // 鼠标移入行
                  onMouseLeave: event => {},
                };
              }}
            />
          </div>
          <div className={styles.contentR}>
            <div className={' borderDivList'}>
              <span className={' borderltems'}>开始时间：{pointsList.startDate}</span>
              <span className={' borderltems'}>结束时间：{pointsList.endDate}</span>
              <span className={' borderltems'}>PSI：{Number(pointsList.psi).toFixed(2) || 0}</span>
            </div>
            <div className={styles.contentTitle}>预测打分统计</div>
            
            <Table  columns={yuche} dataSource={pointsList.predictionStatisticsList} pagination={false} size="small"
            rowClassName={(record, index) => {
              let className = 'light-row';
              if (index % 2 === 1) className = 'dark-row';
              return className;
            }}/>
            <Alert
              className={styles.alert}
              message="由于模型是以特定时期的样本所开发的，其是否能适用于开发样本之外的样本，需要通过PSI指标来进行验证，其反映了验证样本在各分数段的分布与建模样本分布的稳定性。PSI值越小，模型越稳定。一般PSI<0.25则认为模型稳定。"
              type="warning"
              closable
              showIcon
            />
            <div className={styles.echTop2} id="forms32"></div>
            <Table  columns={moxing} dataSource={pointsList.psiList} pagination={false} size="small"
            rowClassName={(record, index) => {
              let className = 'light-row';
              if (index % 2 === 1) className = 'dark-row';
              return className;
          }}/>
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(Detail3);
