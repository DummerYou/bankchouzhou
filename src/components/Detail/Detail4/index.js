import React, { PureComponent, Fragment } from 'react';
import styles from './index.css';
import { Input, Button, Icon, DatePicker, Table, Alert, Tabs,Slider, InputNumber, Row, Col } from 'antd';
import * as echarts from 'echarts';
import { connect } from 'dva';
import moment from 'moment';
import { withRouter } from 'react-router';

const { MonthPicker, RangePicker, WeekPicker } = DatePicker;
const { TabPane } = Tabs;

@connect(({ feature }) => ({
  feature,
}))
class Detail4 extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      items: [],
      pointsList: [],
      defaultActiveKey: '1',
      startDate: moment().subtract(1, 'months'),
      endDate: moment(),
      selectedRowKeys: [0],
      inputValue:1,
      confusionMetrics:{
        truePositiveByThresholds:[],
        falsePositiveByThresholds:[],
        trueNegativeByThresholds:[],
        falseNegativeByThresholds:[],
      }
    };
  }

  componentDidMount() {
    // 基于准备好的dom，初始化echarts实例

    this.setDispatch1();
    this.setDispatch2();
  }
  setDispatch1 = () => {
    const {
      dispatch,
      location: { query },
    } = this.props;
    const {
      startDate,
      endDate
    } = this.state;
    dispatch({
      type: 'feature/validityHistogram',
      payload: {
        endDate: moment(endDate).format('YYYY-MM-DD HH:mm:ss'),
        startDate: moment(startDate).format('YYYY-MM-DD HH:mm:ss'),
        serverId: query.id,
      },
    }).then(res => {
      console.log(res);
      if (res.code == 200) {
        var data2 = res.data;
        this.setEcharts11(data2.positiveAndNegative);
        this.setEcharts12(data2.auc);
        this.setEcharts13(data2.ks);
      }
    });
  };
  setDispatch2 = () => {
    const {
      dispatch,
      location: { query },
    } = this.props;
    const {
      startDate,
      endDate
    } = this.state;
    dispatch({
      type: 'feature/validityQuery',
      payload: {
        serverId: query.id,
        endDate: moment(endDate).format('YYYY-MM-DD HH:mm:ss'),
        startDate: moment(startDate).format('YYYY-MM-DD HH:mm:ss'),
      },
    }).then(res => {
      console.log(res);
      if (res.code == 200) {
        var data2 = res.data;
        this.setState({
          items: data2.data || {},
        });
        if(data2.data[0]){
          this.setpointsList(data2.data[0]);
        }
      }
    });
  };
  setEcharts11 = data => {
    console.log(data);
    var myChart1 = echarts.init(document.getElementById('forms1'));
    myChart1.clear();
    let option = {
      grid: {
        top: '20px',
        left: '50px',
        right: '0',
        bottom: '50px',
      },
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'shadow',
        },
        // formatter: function(params){
        //   console.log(params);
        //   // return params[0].name + "</br>" + params.map((v)=>{
        //   //   return  v.marker + v.seriesName + '&nbsp;&nbsp;&nbsp;&nbsp;' +  v.data
        //   // }).join("</br>");
        // },
      },
      xAxis: {
        type: 'category',
        axisTick: {
          alignWithLabel: true,
        },
        data: data.xAxis,
      },
      yAxis: {
        type: 'value',
      },
      series: [],
    };
    for (const key in data.series) {
      console.log(key);
      option.series.push({
        name: key,
        type: 'bar',
        data: data.series[key],
      });
    }
    myChart1.setOption(option);
  };
  setEcharts12 = data => {
    console.log(data);
    var myChart1 = echarts.init(document.getElementById('forms2'));
    myChart1.clear();
    let option = {
      grid: {
        top: '20px',
        left: '50px',
        right: '0',
        bottom: '50px',
      },
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'shadow',
        },
        formatter: function(params){
          return params.map((v)=>{
            return v.name + "</br>" + v.marker + v.seriesName + '&nbsp;&nbsp;&nbsp;&nbsp;' +  Number(v.data).toFixed(3)
          }).join("</br>");
        },
      },
      xAxis: {
        type: 'category',
        axisTick: {
          alignWithLabel: true,
        },
        data: data.xAxis,
      },
      yAxis: {
        type: 'value',
      },
      series: {
        name: 'AUC',
        type: 'bar',
        data: data.series,
      },
    };
    console.log(option);
    myChart1.setOption(option);
  };
  setEcharts13 = data => {
    console.log(data);
    var myChart1 = echarts.init(document.getElementById('forms3'));
    myChart1.clear();
    let option = {
      grid: {
        top: '20px',
        left: '50px',
        right: '0',
        bottom: '50px',
      },

      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'shadow',
        },
        formatter: function(params){
          return params.map((v)=>{
            return v.name + "</br>" + v.marker + v.seriesName + '&nbsp;&nbsp;&nbsp;&nbsp;' +  Number(v.data).toFixed(3)
          }).join("</br>");
        },
      },
      xAxis: {
        type: 'category',
        axisTick: {
          alignWithLabel: true,
        },
        data: data.xAxis,
      },
      yAxis: {
        type: 'value',
      },
      series: {
        name: 'KS',
        type: 'bar',
        data: data.series,
      },
    };
    console.log(option);
    myChart1.setOption(option);
  };
  setEcharts2 = (data, name) => {
    console.log(data);
    var myChart4 = echarts.init(document.getElementById('forms4'));
    myChart4.clear();
    let option = {
      grid: {
        top: '20px',
        left: '50px',
        right: '20px',
        bottom: '50px',
      },

      // tooltip: {
      //   order: 'valueDesc',
      //   trigger: 'axis',
      // },
      dataZoom: [
        {
          type: 'inside', //详细配置可见echarts官网
        },
      ],
      xAxis: {
        type: 'category',
        data: data.xlist,
        minInterval: 0.000001,
        axisTick: {
          alignWithLabel: true,
        },
      },
      yAxis: {
        type: 'value',
      },
      series: {
        name: name,
        type: 'line',
        data: data.ylist,
        smooth: true,
      },
    };
    if (name === 'ks') {
      option = {
        grid: {
          top: '20px',
          left: '50px',
          right: '20px',
          bottom: '50px',
        },

        // tooltip: {
        //   order: 'valueDesc',
        //   trigger: 'axis',
        // },
        dataZoom: [
          {
            type: 'inside', //详细配置可见echarts官网
          },
        ],
        xAxis: [
          {
            type: 'category',
            data: data.cumulativeBadByThresholds.xlist,
          },
          {
            type: 'category',
            data: data.cumulativeGoodByThresholds.xlist,
            offset: 130,
          },
          {
            type: 'category',
            data: data.cumulativeKSDiff.xlist,
            offset: 130,
          },
        ],
        yAxis: {
          type: 'value',
        },
        series: [
          {
            name: 'cumulativeBadByThresholds',
            type: 'line',
            smooth: true,
            emphasis: {
              focus: 'series',
            },
            data: data.cumulativeBadByThresholds.ylist,
          },
          {
            name: 'cumulativeGoodByThresholds',
            type: 'line',
            smooth: true,
            xAxisIndex: 1,
            emphasis: {
              focus: 'series',
            },
            data: data.cumulativeGoodByThresholds.ylist,
          },
          {
            name: 'cumulativeKSDiff',
            type: 'line',
            smooth: true,
            xAxisIndex: 2,
            emphasis: {
              focus: 'series',
            },
            data: data.cumulativeKSDiff.ylist,
          },
        ],
      };
    }
    // for (const key in data.ylist[0]) {
    //   console.log(key)
    //   option.series.push({
    //     name: key,
    //         type: 'bar',
    //         data: data.ylist[0][key]
    //   })
    // }
    myChart4.setOption(option);
  };
  onChangeSlider = value => {
    this.setState({
      inputValue: value,
    });
  };
  onChange1 = (date, dateString) => {
    console.log(date, dateString);
    this.setState({
      startDate:date
    },()=>{
      this.setDispatch1();
    this.setDispatch2();
    });
  };
  onChange2 = (date, dateString) => {
    console.log(date, dateString);
    this.setState({
      endDate:date
    },()=>{
      this.setDispatch1();
    this.setDispatch2();
    });
  };
  setpointsList = data => {
    console.log(data);
    this.setState({
      pointsList: data,
      confusionMetrics:data.confusionMatrix,
      inputValue: 1,
    });
  };
  callbackTabs = key => {
    const { pointsList } = this.state;
    console.log(key);
    this.setState({
      defaultActiveKey: key,
    });
    console.log(pointsList[key]);
    if (key !== '1') {
      setTimeout(() => {
        this.setEcharts2(pointsList[key], key);
      }, 100);
    }
  };
  getPercent(num1, num2) {
      num1 = parseFloat(num1);
      num2 = parseFloat(num2);
      if (isNaN(num1) || isNaN(num2)) {
          return "-";
      }
      return Math.round(num2 / num1 * 10000) / 100 + "%";;
  }
  render() {
    const { items, pointsList,confusionMetrics, defaultActiveKey,startDate,endDate  ,inputValue} = this.state;

    let confus = confusionMetrics
    if(!confus){
      confus = {
        truePositiveByThresholds:[],
        falsePositiveByThresholds:[],
        trueNegativeByThresholds:[],
        falseNegativeByThresholds:[],
      }
    }
    let slider1 = confus.truePositiveByThresholds[inputValue-1];
    let slider2 = confus.falseNegativeByThresholds[inputValue-1];
    let slider3 = confus.falsePositiveByThresholds[inputValue-1];
    let slider4 = confus.trueNegativeByThresholds[inputValue-1];
    let slider5 = confus.truePositiveByThresholds[inputValue-1] + 
                  confus.trueNegativeByThresholds[inputValue-1]+
                  confus.falsePositiveByThresholds[inputValue-1]+
                  confus.falseNegativeByThresholds[inputValue-1];
    let sliderAll = pointsList.instanceMetrics || {}
    console.log(sliderAll);
    const leftcolumns = [
      {
        title: '计算时间',
        dataIndex: 'startTime',
        key: 'startTime',
      },
      {
        title: 'AUC',
        dataIndex: 'auc',
        key: 'auc',
        render: (text, record) => (
          Number(text).toFixed(2) 
        ),
      },
    ];
    const dafenTj = [
      {
        title: '名称',
        dataIndex: 'firstName',
        key: 'firstName',
      },
      {
        title: '类型',
        dataIndex: 'lastName',
        key: 'lastName',
      },
      {
        title: 'IV',
        dataIndex: 'address',
        key: 'address',
      },
      {
        title: '分箱数',
        dataIndex: 'age',
        key: 'age',
      },
    ];
    const moxing = [
      {
        title: 'score分组',
        dataIndex: 'f1score',
        key: 'f1score',
        render: (text, record,index) => (
          index+1
        ),
      },
      {
        title: '样本占比',
        dataIndex: 'instanceRatio',
        key: 'instanceRatio',
        render: (text, record) => (
          Number(text).toFixed(2) +"%"
        ),
      },
      {
        title: '真正例',
        dataIndex: 'numTruePositives',
        key: 'numTruePositives',
      },
      {
        title: '伪正例',
        dataIndex: 'numFalsePositives',
        key: 'numFalsePositives',
      },
      {
        title: '伪负例',
        dataIndex: 'numFalseNegatives',
        key: 'numFalseNegatives',
      },
      {
        title: '真负例',
        dataIndex: 'numTrueNegatives',
        key: 'numTrueNegatives',
      },
      {
        title: '准确率',
        dataIndex: 'accuracy',
        key: 'accuracy',
        render: (text, record) => (
          Number(text).toFixed(2) +"%"
        ),
      },
      {
        title: '召回',
        dataIndex: 'recall',
        key: 'recall',
      },
      {
        title: 'Specificity',
        dataIndex: 'specifity',
        key: 'specifity',
      },
      {
        title: '本组AUC',
        dataIndex: 'binAuc',
        key: 'binAuc',
        render: (text, record) => (
          Number(text).toFixed(4)
        ),
      },
      {
        title: '累积AUC',
        dataIndex: 'cumulativeAuc',
        key: 'cumulativeAuc',
        render: (text, record) => (
          Number(text).toFixed(4)
        ),
      },
    ];

    const data = [];
    return (
      <div className={styles.bmq + " mt10"}>
        <div className={'flexLr'}>
          <div className={' borderDivT'}>模型有效性报告</div>
          <div className={styles.right}>
            <span className={styles.mlr10}>开始时间</span>
            <DatePicker value={startDate} onChange={this.onChange1} />
            <span className={styles.mlr10}>结束时间</span>
            <DatePicker value={endDate} onChange={this.onChange2} />
          </div>
        </div>
        
        <div className={styles.echTop}>
          <div className={styles.echTops}>
            <div className={styles.echTopTitle}>正负例变化</div>
            <div className={styles.echTopDiv} id="forms1"></div>
          </div>
          <div className={styles.echTops}>
            <div className={styles.echTopTitle}>AUC变化</div>
            <div className={styles.echTopDiv} id="forms2"></div>
          </div>
          <div className={styles.echTops}>
            <div className={styles.echTopTitle}>KS变化</div>
            <div className={styles.echTopDiv} id="forms3"></div>
          </div>
        </div>
        <div className={styles.content}>
          <div className={styles.contentL}>
            <Table
              bordered
              columns={leftcolumns}
              dataSource={items}
              pagination={false}
              className={"radioTable"}
              rowSelection={{
                type: 'radio',
                columnWidth:"1px",
                onChange:  (selectedRowKeys, selectedRows) => {
                  console.log(selectedRowKeys);
                  this.setState({
                    selectedRowKeys: selectedRowKeys,
                  });
                },
                selectedRowKeys: this.state.selectedRowKeys,
              }}
              onRow={(record,index ) => {
                return {
                  onClick: event => {
                    this.setpointsList(record);
                    this.setState({
                      selectedRowKeys: [index],
                    });
                  }, // 点击行
                  onDoubleClick: event => {},
                  onContextMenu: event => {},
                  onMouseEnter: event => {}, // 鼠标移入行
                  onMouseLeave: event => {},
                };
              }}
            />
          </div>
          <div className={styles.contentR}>
            <div className={' borderDivList'}>
              <span className={' borderltems'}>开始时间：{pointsList.startTime}</span>
              <span className={' borderltems'}>结束时间：{pointsList.endTime}</span>
              <span className={' borderltems'}>AUC：{Number(pointsList.auc).toFixed(2) || 0}</span>
            </div>
            <Tabs activeKey={defaultActiveKey} onChange={this.callbackTabs}>
              <TabPane tab="业务效果报告（PR）" key="1"></TabPane>
              <TabPane tab="ROC图" key="roc"></TabPane>
              <TabPane tab="Lift图" key="liftByThresholds"></TabPane>
              <TabPane tab="KS图" key="ks"></TabPane>
              <TabPane tab="Gain图" key="gainByThresholds"></TabPane>
              <TabPane tab="Recall图" key="recallByThresholds"></TabPane>
            </Tabs>
            <Alert
              className={styles.alert}
              message="命中率(准确率)和覆盖率(召回率)是用来评价模型质量的两个重要指标。其中命中率是指模型预测结果为正且实际结果也为正数据的概率；覆盖率是指模型预测为正且实际也为正的数据，在所有实际为正的数据中的占比。两者取值在0和1之间，数值越接近1，表示命中率或覆盖率就越高，一般代表业务效果越好。"
              type="warning"
              closable
              showIcon
            />
            <div className={styles.sliderDiv + (defaultActiveKey === '1' ? ' disyes' : ' disNo')}>
              <div className={styles.RowDiv}>
                <div className={styles.RowName}>
                  调整覆盖率
                </div>
                <div className={styles.RowSlider}>
                  <Slider
                    min={1}
                    max={100}
                    onChange={this.onChangeSlider}
                    value={typeof inputValue === 'number' ? inputValue : 0}
                  />
                </div>
                <div className={styles.RowInput}>
                  <InputNumber
                    min={1}
                    max={100}
                    formatter={value => `${value}%`}
                    parser={value => value.replace('%', '')}
                    style={{ marginLeft: 16 }}
                    value={inputValue}
                    onChange={this.onChangeSlider}
                  />
                </div>
              </div>
              <div className={styles.tables}>
                <div className={styles.tables1}></div>
                <div className={styles.tables2}>模型预测（正）</div>
                <div className={styles.tables2}>模型预测（负）</div>
                <div className={styles.tables2}>总数</div>
              </div>
              <div className={styles.tables}>
                <div className={styles.tables1}>实际值（正）</div>
                <div className={styles.tables2 + " " + styles.tablesNum + " " + styles.tablescolor1}>{slider1}<span>{this.getPercent(slider5,slider1)}</span></div>
                <div className={styles.tables2 + " " + styles.tablesNum + " " + styles.tablescolor2}>{slider2}<span>{this.getPercent(slider5,slider2)}</span></div>
                <div className={styles.tables2 + " " + styles.tablesNum + " " + styles.tablescolor3}>{sliderAll.positiveInstance}<span>{this.getPercent(sliderAll.totalInstance,sliderAll.positiveInstance)}</span></div>
              </div>
              <div className={styles.tables}>
                <div className={styles.tables1}>实际值（负）</div>
                <div className={styles.tables2 + " " + styles.tablesNum + " " + styles.tablescolor2}>{slider3}<span>{this.getPercent(slider5,slider3)}</span></div>
                <div className={styles.tables2 + " " + styles.tablesNum + " " + styles.tablescolor1}>{slider4}<span>{this.getPercent(slider5,slider4)}</span></div>
                <div className={styles.tables2 + " " + styles.tablesNum + " " + styles.tablescolor3}>{sliderAll.negativeInstance}<span>{this.getPercent(sliderAll.totalInstance,sliderAll.negativeInstance)}</span></div>
              </div>
              <div className={styles.tables}>
                <div className={styles.tables1}>总数</div>
                <div className={styles.tables2 + " " + styles.tablesNum + " " + styles.tablescolor3}>{slider1 + slider3}<span>{this.getPercent(slider5,(slider1 + slider3))}</span></div>
                <div className={styles.tables2 + " " + styles.tablesNum + " " + styles.tablescolor3}>{slider2 + slider4}<span>{this.getPercent(slider5,(slider2 + slider4))}</span></div>
              </div>
            </div>
            <div
              className={styles.echTop2 + (defaultActiveKey === '1' ? ' disNo' : ' disyes')}
              id="forms4"
            ></div>
            <Alert
              className={styles.alert}
              message="下方演算表格从样本量占比角度来将数据分为了十组，计算各组以及累计的命中率与覆盖率，具体分组逻辑为，按照模型打分降序排列，从头开始按照分组数据占比依次取走每组数据(不放回)。"
              type="warning"
              closable
              showIcon
            />
            <Table
              columns={moxing}
              dataSource={pointsList.groupedMetricsVoList}
              pagination={false}
              size="small"
              rowClassName={(record, index) => {
                let className = 'light-row';
                if (index % 2 === 1) className = 'dark-row';
                return className;
              }}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(Detail4);
