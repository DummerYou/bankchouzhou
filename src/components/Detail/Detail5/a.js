let dataJson = {
    "code": 200,
    "message": "请求成功",
    "data": {
      "page": 1,
      "size": 10,
      "total": 2,
      "data": [
        {
          "startTime": "2021-07-21 16:18:02",//开始时间
          "pointsList": [//分箱对象
            {
              "name": "age",//名称
              "type": "int64",//类型
              "number": 9,//分箱数
              "iv": 0.2609993598071401//iv值
            },
            {
              "name": "id",
              "type": "int64",
              "number": 4,
              "iv": 0.0012575015806768793
            },
            {
              "name": "RevolvingUtilizationOfUnsecuredLines",
              "type": "float64",
              "number": 7,
              "iv": 1.1038456349367043
            },
            {
              "name": "NumberOfTime30-59DaysPastDueNotWorse",
              "type": "int64",
              "number": 3,
              "iv": 0.7404812872794013
            },
            {
              "name": "DebtRatio",
              "type": "float64",
              "number": 8,
              "iv": 0.07194770491642424
            },
            {
              "name": "MonthlyIncome",
              "type": "int64",
              "number": 8,
              "iv": 0.07298354581976553
            },
            {
              "name": "NumberOfOpenCreditLinesAndLoans",
              "type": "int64",
              "number": 7,
              "iv": 0.08460494832381253
            },
            {
              "name": "NumberOfTimes90DaysLate",
              "type": "int64",
              "number": 2,
              "iv": 0.8375513427285136
            },
            {
              "name": "NumberRealEstateLoansOrLines",
              "type": "int64",
              "number": 4,
              "iv": 0.055353865432772764
            },
            {
              "name": "NumberOfTime60-89DaysPastDueNotWorse",
              "type": "int64",
              "number": 2,
              "iv": 0.5723728876090994
            },
            {
              "name": "NumberOfDependents",
              "type": "int64",
              "number": 4,
              "iv": 0.033818251077554096
            }
          ]
        },
        {
          "startTime": "2021-07-20 15:03:01",
          "pointsList": [
            {
              "name": "age",
              "type": "int64213",
              "number": 9213,
              "iv": 0
            },
            {
              "name": "id",
              "type": "int64",
              "number": 4,
              "iv": 0.0012575015806768793
            },
            {
              "name": "RevolvingUtilizationOfUnsecuredLines",
              "type": "float64",
              "number": 7,
              "iv": 1.1038456349367043
            },
            {
              "name": "NumberOfTime30-59DaysPastDueNotWorse",
              "type": "int64",
              "number": 3,
              "iv": 0.7404812872794013
            },
            {
              "name": "DebtRatio",
              "type": "float64",
              "number": 8,
              "iv": 0.07194770491642424
            },
            {
              "name": "MonthlyIncome",
              "type": "int64",
              "number": 8,
              "iv": 0.07298354581976553
            },
            {
              "name": "NumberOfOpenCreditLinesAndLoans",
              "type": "int64",
              "number": 7,
              "iv": 0.08460494832381253
            },
            {
              "name": "NumberOfTimes90DaysLate",
              "type": "int64",
              "number": 2,
              "iv": 0.8375513427285136
            },
            {
              "name": "NumberRealEstateLoansOrLines",
              "type": "int64",
              "number": 4,
              "iv": 0.055353865432772764
            },
            {
              "name": "NumberOfTime60-89DaysPastDueNotWorse",
              "type": "int64",
              "number": 2,
              "iv": 0.5723728876090994
            },
            {
              "name": "NumberOfDependents",
              "type": "int64",
              "number": 4,
              "iv": 0.033818251077554096
            }
          ]
        }
      ]
    }
  }
  export default dataJson