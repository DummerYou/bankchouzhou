import React, { PureComponent, Fragment } from 'react';
import styles from './index.css';
import { Input, Button, Icon, DatePicker ,Table } from 'antd';
import { connect } from 'dva';
import moment from 'moment';
import { withRouter } from 'react-router';
import dataJson from './a.js';

@connect(({ feature }) => ({
  feature,
}))
class Detail5 extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      items:[],
      pointsList:[],
      startDate: moment().subtract(1, 'months'),
      endDate: moment(),
      selectedRowKeys: [0],
    };
  }

  componentDidMount() {
    this.setDispatch1()
  }
  setDispatch1 = () => {
    
    const {
      dispatch,
      location: { query },
    } = this.props;

    const {
      startDate,
      endDate
    } = this.state;
    dispatch({
      type: 'feature/points',
      payload: {
        serverId: query.id,
        "current": 1,
        "size": 10,
        endDate: moment(endDate).format('YYYY-MM-DD HH:mm:ss'),
        startDate: moment(startDate).format('YYYY-MM-DD HH:mm:ss'),
      },
    }).then(res => {
      console.log(res);
      if (res.code == 200) {
        var data = res.data;
        this.setState({
          items: data.data || {},
        });
        if(data.data[0]){
          this.setpointsList(data.data[0]);
        }
      }else{
        var data2 = dataJson.data;
        this.setState({
          items: data2.data || {}
        });
      }
    });
  };
  setpointsList = data =>{
    console.log(data.pointsList)
    this.setState({
      pointsList:data.pointsList
    });
  }
  
  onChange1 = (date, dateString) => {
    console.log(date, dateString);
    this.setState({
      startDate:date
    },()=>{
      this.setDispatch1();
    });
  };
  onChange2 = (date, dateString) => {
    console.log(date, dateString);
    this.setState({
      endDate:date
    },()=>{
      this.setDispatch1();
    });
  };
  

  render() {
    const { items ,pointsList ,startDate,endDate } = this.state;
    console.log(items)
    const columns = [
      {
        title: '计算时间',
        dataIndex: 'startTime',
        key: 'startTime',
      },
    ];
    const columns2 = [
      {
        title: '名称',
        dataIndex: 'name',
        key: 'name',
      },
      {
        title: '类型',
        dataIndex: 'type',
        key: 'type',
      },
      {
        title: 'IV',
        dataIndex: 'iv',
        key: 'iv',
      },
      {
        title: '分箱数',
        dataIndex: 'number',
        key: 'number',
      },
    ];
    
    const data = [
      {
        key: '1',
        firstName: 'John',
        lastName: 'Brown',
        age: 32,
        address: '0.2',
        time: '2021-7-17 14:24:01',
      },
      {
        key: '2',
        firstName: 'Jim',
        lastName: 'Green',
        age: 42,
        address: '0.2',
        time: '2021-7-17 14:24:01',
      },
      {
        key: '3',
        firstName: 'Joe',
        lastName: 'Black',
        age: 32,
        address: '0.2',
        time: '2021-7-17 14:24:01',
      },
    ];
    return (
      <div className={styles.bmq + " mt10"}>
        <div className={'flexLr'}>
          <div className={' borderDivT'}>模型变量分箱报告</div>
          <div className={styles.righttop}>
            <span className={styles.mlr10}>开始时间</span>
            <DatePicker value={startDate} onChange={this.onChange1} />
            <span className={styles.mlr10}>结束时间</span>
            <DatePicker value={endDate} onChange={this.onChange2} />
          </div>
        </div>
        
        <div className={styles.bottom}>
        <div className={styles.left}>
        <Table  bordered columns={columns} dataSource={items} pagination={false} 
        
        className={"radioTable"}
        rowSelection={{
          type: 'radio',
          columnWidth:"1px",
          onChange:  (selectedRowKeys, selectedRows) => {
            console.log(selectedRowKeys);
            this.setState({
              selectedRowKeys: selectedRowKeys,
            });
          },
          selectedRowKeys: this.state.selectedRowKeys,
        }}
        onRow={(record,index ) => {
          return {
            onClick: event => {
              this.setpointsList(record);
              this.setState({
                selectedRowKeys: [index],
              });
            }, // 点击行
            onDoubleClick: event => {},
            onContextMenu: event => {},
            onMouseEnter: event => {}, // 鼠标移入行
            onMouseLeave: event => {},
          };
        }}/>
        </div>
        <div className={styles.right}>
        <Table  bordered columns={columns2} dataSource={pointsList} pagination={false} />
        </div>
        </div>
      </div>
    );
  }
}

export default withRouter(Detail5);