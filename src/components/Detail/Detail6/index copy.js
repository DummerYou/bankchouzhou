import React, { PureComponent, Fragment } from 'react';
import styles from './index.css';
import { Input, InputNumber , Button, Icon, DatePicker ,Table,Form,Radio,Select } from 'antd';
import { connect } from 'dva';
import moment from 'moment';
const { Option } = Select;
const { MonthPicker, RangePicker, WeekPicker } = DatePicker;
const { TextArea } = Input;


const selectAfter = (
  <Select defaultValue=".com" style={{ width: 80 }}>
    <Option value=".com">.com</Option>
    <Option value=".jp">.jp</Option>
    <Option value=".cn">.cn</Option>
    <Option value=".org">.org</Option>
  </Select>
);



@connect(({ feature }) => ({
  feature,
}))
@Form.create()
class addItem3 extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      dagData:[],
      checkbox2:"",
    };
  }

  componentDidMount() {
    
    const { dispatch} = this.props;
    dispatch({
      type: 'feature/getDag',
      payload: {
        page:1, 
        size:100
      },
    }).then(res => {
      if (res.code == 200) {
        // message.error(res.msg);
        console.log(res)
        this.setState({ dagData:res.data });
      } else {
        
      }
    });
  }
   handleChange = (value) => {
    console.log(`selected ${value}`);
  }
  handleChange1 = (value) => {
    console.log(`selected ${value}`);
    this.setState({
      checkbox2: value,
    });
  }
  onChange = (date, dateString) => {
    console.log(date, dateString);
  }
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
      }
    });
  };

  render() {
    const { form } = this.props;
    const { getFieldDecorator } = form;
    const {
      checkbox2
    } = this.state;
    
    const formItemLayout = {
      layout:"vertical"
    };
    return (
      <div className={styles.bmq}>
        <Form   {...formItemLayout} onSubmit={this.handleSubmit}>
          <Form.Item  label="配置模式">
            {getFieldDecorator('checkbox1', {
            initialValue: "a",
          })(
              <Radio.Group>
                <Radio value="a">表单配置</Radio>
                <Radio value="b">自定义dag</Radio>
              </Radio.Group>,
            )}
          </Form.Item>
          <Form.Item  label="模型稳定性监控"></Form.Item>
          <Form.Item  label="模型PSI基准数据">
            {getFieldDecorator('checkbox2', {
            initialValue: "a",
          })(
              <Radio.Group>
                <Radio value="a">线上第一次评分</Radio>
                <Radio value={checkbox2}>选择数据
                  <Select  style={{ width: 120 }} onChange={this.handleChange1}>
                    <Option value="jack">Jack</Option>
                    <Option value="lucy">Lucy</Option>
                    <Option value="Yiminghe">yiminghe</Option>
                  </Select>
                </Radio>
              </Radio.Group>,
            )}
          </Form.Item>
          <Form.Item  label="模型有效性监控"></Form.Item>
          <Form.Item  label="反馈数据组">
            {getFieldDecorator('Select1')(
              <Select  onChange={this.handleChange}>
                <Option value="jack">Jack</Option>
                <Option value="lucy">Lucy</Option>
                <Option value="Yiminghe">yiminghe</Option>
              </Select>,
            )}
          </Form.Item>
          <Form.Item  label="数据切片获取">
            {getFieldDecorator('Select3')(
              <Select  onChange={this.handleChange}>
              <Option value="jack">Jack</Option>
              <Option value="lucy">Lucy</Option>
              <Option value="Yiminghe">yiminghe</Option>
            </Select>,
            )}
          </Form.Item>
          <Form.Item  label="起始时间">
            {getFieldDecorator('input1')(
              //  <InputNumber min={0}  precision={0} addonAfter="天前" defaultValue="mysite" />,
               <Input  addonAfter="天前" />,
            )}
          </Form.Item>
          <Form.Item  label="截止时间">
            {getFieldDecorator('checkbox5', {
            initialValue: "a",
          })(
              <Radio.Group>
              <Radio value="a">最早上传的数据切片</Radio>
              <Radio value="b"><Input  addonAfter="天前" /></Radio>
            </Radio.Group>,
            )}
            <div>当前工获取到1片有效数据</div>
          </Form.Item>

          <Form.Item  label="反馈数据滞后到达时间">
            {getFieldDecorator('input1')(
              //  <InputNumber min={0}  precision={0} addonAfter="天前" defaultValue="mysite" />,
               <Input   addonAfter={selectAfter}  />,
            )}
          </Form.Item>

          <Form.Item  label="模型变量监控"></Form.Item>
          <Form.Item  label="特征字段选择">
            {getFieldDecorator('checkbox7', {
            initialValue: "a",
          })(
              <Radio.Group>
                <Radio value="a">选择全部字段</Radio><br />
                <Radio value="b">自定义<br />
                    <TextArea
                    placeholder=""
                    autoSize={{ minRows: 3, maxRows: 5 }}
                  />
                </Radio>
              </Radio.Group>,
            )}
          </Form.Item>
          <Form.Item  label="目标值字段选择">
            {getFieldDecorator('input8')(
               <Input  />,
            )}
          </Form.Item>
          <Form.Item  label="分籍方式">
            {getFieldDecorator('Select8')(
              <Select  onChange={this.handleChange}>
              <Option value="jack">Jack</Option>
              <Option value="lucy">Lucy</Option>
              <Option value="Yiminghe">yiminghe</Option>
            </Select>,
            )}
          </Form.Item>
          <Form.Item  label="分籍个数">
            {getFieldDecorator('input9')(
               <Input  />,
            )}
          </Form.Item>
          <Form.Item  label="自定义分籍">
            {getFieldDecorator('input10')(
               <TextArea
               placeholder=""
               autoSize={{ minRows: 3, maxRows: 5 }}
             />,
            )}
          </Form.Item>
          
          <Form.Item  label="监控运行策略"></Form.Item>
          <Form.Item  label="运行模式">
            {getFieldDecorator('checkbox6', {
            initialValue: "a",
          })(
              <Radio.Group>
                <Radio value="a">单次运行</Radio>
                <Radio value="b">循环运行</Radio>
                <Radio value="c">crontab表达式</Radio>
              </Radio.Group>,
            )}
          </Form.Item>
          <Form.Item  label="循环起始于">
            {getFieldDecorator('input1')(
              //  <InputNumber min={0}  precision={0} addonAfter="天前" defaultValue="mysite" />,
              <DatePicker format="YYYY-MM-DD HH:mm:ss" onChange={this.onChange} />,
            )}
          </Form.Item>
          <Form.Item  label="循环周期">
            {getFieldDecorator('input1')(
              //  <InputNumber min={0}  precision={0} addonAfter="天前" defaultValue="mysite" />,
               <Input  addonAfter={selectAfter} />,
            )}
          </Form.Item>
          <Form.Item  label="循环终止规则">
          {getFieldDecorator('checkbox2', {
            initialValue: "a",
          })(
              <Radio.Group>
                <Radio value="a">一直循环</Radio>
                <Radio value="b">循环终止于
                  <DatePicker format="YYYY-MM-DD HH:mm:ss" onChange={this.onChange} />
                </Radio>
                <Radio value="b">循环次数
                  <Input />,
                </Radio>
              </Radio.Group>,
            )}
          </Form.Item>
          <Form.Item  label="任务超时时长">
            {getFieldDecorator('input1')(
              //  <InputNumber min={0}  precision={0} addonAfter="天前" defaultValue="mysite" />,
               <Input  addonAfter={selectAfter} />,
            )}
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit">保存</Button>
          </Form.Item>
        </Form>
      </div>
    );
  }
}

export default addItem3;