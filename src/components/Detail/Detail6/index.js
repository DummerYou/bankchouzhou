import React, { PureComponent, Fragment } from 'react';
import styles from './index.css';
import { Input, InputNumber, Button, Icon, DatePicker, message, Form, Radio, Select } from 'antd';
import { connect } from 'dva';
import router from 'umi/router';
import { withRouter } from 'react-router';
import moment from 'moment';
const { Option } = Select;
const { MonthPicker, RangePicker, WeekPicker } = DatePicker;
const { TextArea } = Input;

@connect(({ feature }) => ({
  feature,
}))
@Form.create()
class Detail6 extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      data:[],
      dagData1: [],
      dagData2: [],
      checkbox2: '',
      selectAfterV: '1',
      dataItems:{},
    };
  }

  componentDidMount() {
    const {
      dispatch,
      location: { query },
    } = this.props;
    dispatch({
      type: 'feature/modelDetails',
      payload: {
        serverId:query.id, 
      },
    }).then(res => {
      if (res.code == 200) {
        // message.error(res.msg);
        console.log(res)
        this.setState({ dataItems:res.data || {} });
        this.setState({ selectAfterV:res.data.selectAfterV || "1" });
        
      } else {
        
      }
    });
    dispatch({
      type: 'feature/getForecastServer',
      payload: {
        page:1, 
        size:100
      },
    }).then(res => {
      if (res.code == 200) {
        // message.error(res.msg);
        console.log(res)
        this.setState({ data:res.data.data });
      } else {
        
      }
    });
    dispatch({
      type: 'feature/getDag1',
      payload: {
        page: 1,
        size: 100,
      },
    }).then(res => {
      if (res.code == 200) {
        // message.error(res.msg);
        console.log(res);
        this.setState({ dagData1: res.data });
      } else {
      }
    });
    dispatch({
      type: 'feature/getDag2',
      payload: {
        page: 1,
        size: 100,
      },
    }).then(res => {
      if (res.code == 200) {
        // message.error(res.msg);
        console.log(res);
        this.setState({ dagData2: res.data });
      } else {
      }
    });
  }
  handleChange = value => {
    console.log(`selected ${value}`);
  };
  handleChange1 = value => {
    console.log(`selected ${value}`);
    this.setState({
      checkbox2: value,
    });
  };
  onChange = (date, dateString) => {
    console.log(date, dateString);
  };
  handleSubmit = e => {
    const { dispatch } = this.props;
    const { selectAfterV } = this.state;
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        console.log(values);
        var timeInter = values.input1;
        const datas = {
          // createdBy: values.effectivenessDag,
          effectivenessDag: values.effectivenessDag,
          stabilityDag: values.stabilityDag,
          targetServerId: timeInter.key,
          targetServerName: timeInter.label,
          timeInterval: values.timeInterval,
          timeOutSeconds: values.timeOutSeconds*60,
          timeUnit: selectAfterV*1,
        };
        
        console.log(datas)

        dispatch({
          type: 'feature/save',
          payload: datas,
        }).then(res => {
          console.log(res)
          if (res.code == 200) {
            // message.error(res.msg);
            message.success(res.message || "添加成功！");
            router.push('/');
          } else {
            message.error(res.message || "添加失败！");
          }
        });
      }
    });
  };
  checkPrice = (rule, value, callback) => {
    console.log(value);
    if (value > 0) {
      return callback();
    }
    callback('请输入数字!');
  };
  selectAfterC = key => {
    console.log(key);
    this.setState({
      selectAfterV: key,
    });
  };

  render() {
    const { form } = this.props;
    const { getFieldDecorator } = form;
    const { checkbox2,data, dagData1,dagData2, selectAfterV ,dataItems} = this.state;

    const formItemLayout = {
      layout: 'vertical',
    };
    const selectAfter = (
      <Select value={selectAfterV} style={{ width: 80 }} onChange={this.selectAfterC} disabled>
        <Option value="1">小时</Option>
        <Option value="2">天</Option>
      </Select>
    );

    const selectAfter2 = (
      <Select value="分" style={{ width: 80 }} disabled>
        <Option value="分">分</Option>
      </Select>
    );
    return (
      <div className={styles.bmq + ' noForm borderDiv'}>
        <Form {...formItemLayout} className={' w80'} onSubmit={this.handleSubmit}>
        <Form.Item label="模型预估应用">
          {getFieldDecorator('input1', {
                        initialValue:{key: dataItems.targetServerId, label: dataItems.targetServerName}
                    })(
            <Select labelInValue onChange={this.handleChange} disabled>
              {
                data.length?data.map((v)=>{
                  return <Option value={v.id} key={v.id}>{v.name}</Option>
                }):null
              }
          </Select>,
          )}
        </Form.Item>
          <Form.Item label="配置模式">
            <Radio.Group value="b" disabled>
              <Radio value="b">自定义dag</Radio>
            </Radio.Group>
          </Form.Item>
          <Form.Item label="模型稳定性监控">
            {getFieldDecorator('stabilityDag', {
              initialValue:dataItems.stabilityDag,
              rules: [{ required: true, message: '请选择!' }],
            })(
              <Select onChange={this.handleChange} disabled>
                {dagData1.length
                  ? dagData1.map(v => {
                      return (
                        <Option value={v.path} key={v.path}>
                          {v.name}
                        </Option>
                      );
                    })
                  : null}
              </Select>,
            )}
          </Form.Item>

          <Form.Item label="模型有效性和模型变量监控">
            {getFieldDecorator('effectivenessDag', {
              initialValue:dataItems.effectivenessDag,
              rules: [{ required: true, message: '请选择!' }],
            })(
              <Select onChange={this.handleChange} disabled>
                {dagData2.length
                  ? dagData2.map(v => {
                      return (
                        <Option value={v.path} key={v.path}>
                          {v.name}
                        </Option>
                      );
                    })
                  : null}
              </Select>,
            )}
          </Form.Item>

          <Form.Item label="监控运行策略"></Form.Item>
          <Form.Item label="运行模式">
            <Radio.Group value="b" disabled>
              <Radio value="b">循环运行</Radio>
            </Radio.Group>
          </Form.Item>
          <Form.Item label="循环周期">
            {getFieldDecorator('timeInterval', {
              initialValue:dataItems.timeInterval,
              rules: [{ validator: this.checkPrice }],
            })(
              //  <InputNumber min={0}  precision={0} addonAfter="天前" defaultValue="mysite" />,
              <Input addonAfter={selectAfter} disabled/>,
            )}
          </Form.Item>
          <Form.Item label="任务超时时长">
            {getFieldDecorator('timeOutSeconds', {
              initialValue:dataItems.timeOutSeconds,
              rules: [{ validator: this.checkPrice }],
            })(
              //  <InputNumber min={0}  precision={0} addonAfter="天前" defaultValue="mysite" />,
              <Input addonAfter={selectAfter2} disabled/>,
            )}
          </Form.Item>
        </Form>
      </div>
    );
  }
}

export default withRouter(Detail6);
