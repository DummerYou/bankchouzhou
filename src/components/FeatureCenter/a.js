function getPathByKey (value,key,arr){
  let temppath = []
  try{
    function getNodePath(node,index){
      temppath.push(index);
      if (node[key] === value) {
        throw ("GOT IT!");
      }
      if (node.children && node.children.length > 0) {
        for (var i = 0; i < node.children.length; i++) {
          getNodePath(node.children[i],i);
        }
        temppath.pop();
      }else {
        temppath.pop();
      }
    }
    for (let i = 0; i < arr.length; i++) {
      getNodePath(arr[i],i);
    }
  }
  catch (e){
    return temppath;
  }
}

getPathByKey(4,'id', tree)