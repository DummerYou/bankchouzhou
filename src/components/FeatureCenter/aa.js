let dataJson = {
  "code": 200,

  "message": "请求成功",

  "data": [
    {
      "current": 0,

      "size": 0,

      "id": 1,

      "createdBy": "SYSTEM",

      "updatedBy": "SYSTEM",

      "createdTime": "2021-05-28 05:33:12",

      "updatedTime": "2021-05-28 05:33:12",

      "sortField": null,

      "parentId": 0,

      "name": "KFC",

      "type": 0,

      "level": 1,

      "path": null,

      "description": "KFC业务场景沉淀特征",

      "children": [
        {
          "current": 0,

          "size": 0,

          "id": 2,

          "createdBy": "SYSTEM",

          "updatedBy": "SYSTEM",

          "createdTime": "2021-05-28 05:33:58",

          "updatedTime": "2021-05-28 05:36:38",

          "sortField": null,

          "parentId": 1,

          "name": "特征中心",

          "type": 0,

          "level": 2,

          "path": null,

          "description": "KFC业务场景的特征中心",

          "children": [
            {
              "current": 0,

              "size": 0,

              "id": 3,

              "createdBy": "SYSTEM",

              "updatedBy": "SYSTEM",

              "createdTime": "2021-05-28 05:34:45",

              "updatedTime": "2021-05-28 23:09:50",

              "sortField": null,

              "parentId": 2,

              "name": "店铺画像",

              "type": 1,

              "level": 3,

              "path": null,

              "description": "KFC业务的店铺画像信息",

              "children": null,

              "tables": null,

              "createdTimeStart": null,

              "createdTimeEnd": null,

              "updatedTimeStart": null,

              "updatedTimeEnd": null
            },

            {
              "current": 0,

              "size": 0,

              "id": 4,

              "createdBy": "SYSTEM",

              "updatedBy": "SYSTEM",

              "createdTime": "2021-05-28 05:37:02",

              "updatedTime": "2021-05-28 06:08:31",

              "sortField": null,

              "parentId": 2,

              "name": "用户画像",

              "type": 1,

              "level": 3,

              "path": null,

              "description": "KFC业务的用户画像信息",

              "children": null,

              "tables": null,

              "createdTimeStart": null,

              "createdTimeEnd": null,

              "updatedTimeStart": null,

              "updatedTimeEnd": null
            }
          ],

          "tables": null,

          "createdTimeStart": null,

          "createdTimeEnd": null,

          "updatedTimeStart": null,

          "updatedTimeEnd": null
        }
      ],

      "tables": null,

      "createdTimeStart": null,

      "createdTimeEnd": null,

      "updatedTimeStart": null,

      "updatedTimeEnd": null
    }
  ]
}
export default dataJson