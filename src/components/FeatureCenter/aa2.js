let dataJson2 = {
      "code": 200,
      "message": "请求成功",
      "data": {
          "records": [
              {
                  "current": 0,
                  "size": 0,
                  "id": 3,
                  "createdBy": "SYSTEM",
                  "updatedBy": "SYSTEM",
                  "createdTime": "2021-05-28 05:34:45",
                  "updatedTime": "2021-05-28 23:09:50",
                  "sortField": null,
                  "parentId": 2,
                  "name": "店铺画像",
                  "type": 1,
                  "level": 3,
                  "path": "KFC/特征中心/店铺画像",
                  "description": "KFC业务的店铺画像信息",
                  "children": null,
                  "tables": [
                      {
                          "current": 0,
                          "size": 0,
                          "id": 1,
                          "createdBy": "SYSTEM",
                          "updatedBy": "SYSTEM",
                          "createdTime": "2021-05-28 05:49:39",
                          "updatedTime": "2021-06-04 07:39:00",
                          "sortField": null,
                          "featureId": null,
                          "name": "Customer.table",
                          "storageEngine": 0,
                          "connectionType": 88,
                          "connectionConfig": "{\"url\":\"http://172.27.96.215:40121/telamon/v1/tables\",\"tableName\":null,\"type\":\"88\",\"aiosPrn\":\"3cf7e5cd-02d0-446b-a820-fc7160ad0cf6/data_group_shand.table-group/banking_data1.table\",\"aiosWorkspaceId\":\"17\",\"userName\":null,\"password\":null}",
                          "schemaData": [
                              {
                                  "name": "job",
                                  "type": "String",
                                  "description": null
                              },
                              {
                                  "name": "age",
                                  "type": "Int",
                                  "description": "ffff"
                              },
                              {
                                  "name": "month",
                                  "type": "String",
                                  "description": null
                              }
                          ],
                          "recordNum": 382,
                          "dataSize": null,
                          "description": "表说明"
                      },
                      {
                          "current": 0,
                          "size": 0,
                          "id": 4,
                          "createdBy": "SYSTEM",
                          "updatedBy": "SYSTEM",
                          "createdTime": "2021-06-03 00:29:29",
                          "updatedTime": "2021-06-04 07:39:02",
                          "sortField": null,
                          "featureId": null,
                          "name": "Customer.table",
                          "storageEngine": 0,
                          "connectionType": 88,
                          "connectionConfig": "{\"url\":\"http://172.27.96.215:40121/telamon/v1/tables\",\"tableName\":null,\"type\":\"88\",\"aiosPrn\":\"3cf7e5cd-02d0-446b-a820-fc7160ad0cf6/data_group_shand.table-group/banking_data1.table\",\"aiosWorkspaceId\":\"17\",\"userName\":null,\"password\":null}",
                          "schemaData": [
                              {
                                  "name": "job",
                                  "type": "String",
                                  "description": null
                              },
                              {
                                  "name": "age",
                                  "type": "Int",
                                  "description": ""
                              },
                              {
                                  "name": "month",
                                  "type": "String",
                                  "description": null
                              }
                          ],
                          "recordNum": 382,
                          "dataSize": null,
                          "description": null
                      },
                      {
                          "current": 0,
                          "size": 0,
                          "id": 5,
                          "createdBy": "SYSTEM",
                          "updatedBy": "SYSTEM",
                          "createdTime": "2021-06-04 07:47:58",
                          "updatedTime": "2021-06-04 07:47:58",
                          "sortField": null,
                          "featureId": null,
                          "name": "Customer.table",
                          "storageEngine": 0,
                          "connectionType": 88,
                          "connectionConfig": "{\"type\":\"88\",\"url\":\"http://172.27.96.215:40121/telamon/v1/tables\",\"aiosPrn\":\"3cf7e5cd-02d0-446b-a820-fc7160ad0cf6/data_group_shand.table-group/banking_data1.table\",\"aiosWorkspaceId\":17}",
                          "schemaData": [
                              {
                                  "name": "job",
                                  "type": "String",
                                  "description": null
                              },
                              {
                                  "name": "age",
                                  "type": "Int",
                                  "description": ""
                              },
                              {
                                  "name": "month",
                                  "type": "String",
                                  "description": null
                              }
                          ],
                          "recordNum": 382,
                          "dataSize": null,
                          "description": null
                      }
                  ],
                  "createdTimeStart": null,
                  "createdTimeEnd": null,
                  "updatedTimeStart": null,
                  "updatedTimeEnd": null
              },
              {
                  "current": 0,
                  "size": 0,
                  "id": 4,
                  "createdBy": "SYSTEM",
                  "updatedBy": "SYSTEM",
                  "createdTime": "2021-05-28 05:37:02",
                  "updatedTime": "2021-05-28 06:08:31",
                  "sortField": null,
                  "parentId": 2,
                  "name": "用户画像",
                  "type": 1,
                  "level": 3,
                  "path": "KFC/特征中心/用户画像",
                  "description": "KFC业务的用户画像信息",
                  "children": null,
                  "tables": [
                      {
                          "current": 0,
                          "size": 0,
                          "id": 2,
                          "createdBy": "SYSTEM",
                          "updatedBy": "SYSTEM",
                          "createdTime": "2021-05-28 06:08:32",
                          "updatedTime": "2021-06-04 07:39:00",
                          "sortField": null,
                          "featureId": null,
                          "name": "Customer.table",
                          "storageEngine": 0,
                          "connectionType": 88,
                          "connectionConfig": "{\"url\":\"http://172.27.96.215:40121/telamon/v1/tables\",\"tableName\":null,\"type\":\"88\",\"aiosPrn\":\"3cf7e5cd-02d0-446b-a820-fc7160ad0cf6/data_group_shand.table-group/banking_data1.table\",\"aiosWorkspaceId\":\"17\",\"userName\":null,\"password\":null}",
                          "schemaData": [
                              {
                                  "name": "job",
                                  "type": "String",
                                  "description": null
                              },
                              {
                                  "name": "age",
                                  "type": "Int",
                                  "description": null
                              },
                              {
                                  "name": "month",
                                  "type": "String",
                                  "description": null
                              }
                          ],
                          "recordNum": 382,
                          "dataSize": null,
                          "description": "表说明"
                      }
                  ],
                  "createdTimeStart": null,
                  "createdTimeEnd": null,
                  "updatedTimeStart": null,
                  "updatedTimeEnd": null
              }
          ],
          "total": 2,
          "size": 10,
          "current": 1,
          "orders": [],
          "optimizeCountSql": true,
          "hitCount": false,
          "countId": null,
          "maxLimit": null,
          "searchCount": true,
          "pages": 1
      }
  }
export default dataJson2