import React, { PureComponent, Fragment } from 'react';
import styles from './index.css';
import searchCss from '../../assets/search.css';
import { Input, Button, Icon, DatePicker, Table, Modal, Form, Select, message } from 'antd';
import { setCook } from '../../services/utils';
import moment from 'moment';
import router from 'umi/router';
import { connect } from 'dva';
import dataJson2 from './aa2.js';
const { MonthPicker, RangePicker, WeekPicker } = DatePicker;
const { Column, ColumnGroup } = Table;
const { confirm } = Modal;
const { Option } = Select;

@connect(({ feature }) => ({
  feature,
}))
@Form.create()
class FeatureCenter extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      visible1: false,
      visible1State: 0,
      visible1Id: null,
      loading: false,
      id: this.props.id ? this.props.id : null,
      name: '',
      description: '',
      createdTime: [],
      updatedTime: [],
      current: 1,
      size: 10,
      list: [],
      listOld: [],
      listFolder: [],
      listFolderOld: [],
      selectedRowKeys: [],
      modifyText: '',
      listFolderText1: '',
      listFolderText2: '',
      listFolderId: '',
      listFoldererr: '',
      listFolderState: false,
      getEnum: [],
      featureList: {},
    };
  }
  showModal = () => {
    this.setState({
      visible: true,
    });
  };

  handleOk = e => {
    const { featureList } = this.state;
    const { dispatch } = this.props;
    console.log(featureList);
    if (!featureList.id) {
      message.error('请选择特征路径');
      return;
    }
    // this.setState({
    //   visible: false,
    // });
    this.props.form.validateFields(['select', 'description2'], (err, values) => {
      console.log(values);
      if (!err) {
        if (!values.description2) {
          message.error('请输入数据路径');
          return;
        }
        // var connectionConfig = { aiosUrl: '', aiosPrn: '', aiosWorkspaceId: 17 };
        // var connectionConfig = {
        //   aiosUrl: 'http://172.27.96.215:40121/telamon/v1/tables',
        //   aiosPrn:
        //     '3cf7e5cd-02d0-446b-a820-fc7160ad0cf6/data_group_shand.table-group/banking_data1.table',
        //   aiosWorkspaceId: 1,
        // };
        // sessionStorage.setItem('connectionConfig', JSON.stringify(connectionConfig));
        const connectionConfigItem = {
          featureId: featureList.id,
          storageEngine: values.select,
        };
        sessionStorage.setItem('connectionConfig', JSON.stringify(values.description2));
        sessionStorage.setItem('connectionConfigItem', JSON.stringify(connectionConfigItem));
        router.push('/featuresNew');
      }
    });
  };

  handleCancel = e => {
    console.log(e);
    this.props.form.resetFields();
    this.setState({
      visible: false,
      featureList: {},
    });
  };
  showModal1 = () => {
    console.log('object');
    this.setState({
      visible1: true,
      visible1State: 0,
      selectedRowKeys: [],
    });
  };
  showModalmobile = id => {
    this.setState({
      visible1: true,
      visible1State: 1,
      visible1Id: id,
      selectedRowKeys: [],
    });
  };
  handleOk1 = e => {
    const {
      selectedRowKeys,
      listOld,
      listFolder,
      listFolderState,
      listFolderId,
      listFolderText1,
      listFolderText2,
    } = this.state;
    const { dispatch } = this.props;
    console.log(selectedRowKeys[0]);
    if (listFolderState) {
      console.log('object');
      console.log(listFolder);
      console.log(listFolderId);
      console.log(listFolderText1);
      console.log(listFolderText2);
      if (!listFolderText1) {
        this.setState({
          listFoldererr: '请输入特征数据',
        });
        this.forceUpdate();
        return;
      }
      dispatch({
        type: 'feature/saveFusionFeature',
        payload: { description: listFolderText2, name: listFolderText1, parentId: listFolderId },
      }).then(res => {
        if (res.code == 200) {
          console.log(res);
          // message.error(res.msg);
          this.setState({
            visible1: false,
            listFolderState: false,
            listFolderId: '',
            listFolderText1: '',
            listFolderText2: '',
          });
          this.forceUpdate();
          this.getFusionFeature();
        } else {
          this.setState({
            listFoldererr: res.msg,
          });
          this.forceUpdate();
        }
      });
    } else {
      this.setFolderType();
    }
  };
  setFolderType = () => {
    const {
      selectedRowKeys,
      visible1State,
      visible1Id,
      listFolder,
      listFolderOld,
      listFolderText1,
      listFolderText2,
    } = this.state;
    const { dispatch } = this.props;

    if (visible1State == 0) {
      // 新增特征
      console.log('新增特征');
      console.log(listFolder);
      console.log(selectedRowKeys[0]);
      // var fileList = listFolder.filter(item => {
      //   return item.id == selectedRowKeys[0];
      // });

      var lists = this.deepClone(listFolderOld);
      var listOrder = this.getPathByKey(selectedRowKeys[0], 'id', lists) || [];
      console.log(listOrder);
      if (listOrder.length >= 1) {
        var text = 'lists[' + listOrder.join(']["children"][') + ']';
        console.log(text);
        console.log(eval(text));
        this.setState({
          featureList: eval(text) || {},
          visible1: false,
        });
        this.forceUpdate();
      }

      // listFolderText1:"",
      // listFolderText2:"",
      // this.setState({
      //   visible1: false,
      // });
      // router.push('/featuresNew');
    } else if (visible1State == 1) {
      // 移动特征
      console.log('移动特征');
      if (selectedRowKeys[0]) {
        console.log({ id: visible1Id, parentId: selectedRowKeys[0] });
        dispatch({
          type: 'feature/saveFusionFeature',
          payload: { id: visible1Id, parentId: selectedRowKeys[0] },
        }).then(res => {
          if (res.code == 200) {
            console.log(res);
            // message.error(res.msg);
            this.search();
          } else {
          }
        });
      }
    }
  };
  mobileFolder = () => {};
  addFolder = () => {
    const { selectedRowKeys, listOld, listFolder, listFolderOld, listFolderState } = this.state;
    console.log(selectedRowKeys[0]);

    if (listFolderState) {
      return;
    }

    var lists = this.deepClone(listFolderOld);
    var dataItem = {
      createdTime: moment().format('YYYY-MM-DD h:mm:ss'),
      description: '',
      name: '',
      editable: true,
      id: 999999,
    };
    var listOrder = this.getPathByKey(selectedRowKeys[0], 'id', lists) || [];
    console.log(listOrder);
    if (listOrder.length >= 1) {
      var text = 'lists[' + listOrder.join(']["children"][') + ']';
      console.log(text);
      console.log(eval(text));
      if (eval(text).children) {
        eval(text).children.push(dataItem);
      } else {
        eval(text).children = [dataItem];
      }
    } else {
      lists.push(dataItem);
    }
    console.log(lists);

    this.setState({
      listFolderId: selectedRowKeys[0] || 0,
      listFolder: lists,
      selectedRowKeys: [999999],
      listFolderState: true,
    });

    console.log('789');
  };

  handleCancel1 = e => {
    const { listFolderOld } = this.state;
    console.log(e);
    this.setState({
      visible1: false,
      listFolder: listFolderOld,
      selectedRowKeys: [],
      listFolderState: false,
      listFolderText1: '',
      listFolderText2: '',
      listFolderId: '',
    });
  };
  componentDidMount() {
    const { dispatch } = this.props;
    this.search();
    dispatch({
      type: 'feature/featureGetEnum',
      payload: { name: 'storageEngineEnum' },
    }).then(res => {
      console.log(res);
      if (res.data.length) {
        console.log(res.data);
        // message.error(res.msg);
        this.setState({
          getEnum: res.data,
        });
      } else {
      }
    });
    setCook('headerName', '特征中心');
    this.getFusionFeature();
  }
  getFusionFeature = () => {
    const { dispatch } = this.props;
    dispatch({
      type: 'feature/getTreeFusionFeature',
    }).then(res => {
      if (res.code == 200) {
        console.log(res);
        // message.error(res.msg);
        this.setState({
          listFolder: res.data,
          listFolderOld: res.data,
        });
        console.log(this.state.list);
      } else {
      }
    });
  };
  onChange(date, dateString) {
    console.log(date, dateString);
  }
  onChangeTime1 = (date, dateString) => {
    console.log(date, dateString);
    this.setState({
      createdTime: date,
    });
  };
  onChangeTime2 = (date, dateString) => {
    console.log(date, dateString);
    this.setState({
      updatedTime: date,
    });
  };

  /*
   * 设置表格单选状态
   * **/
  getCheckboxProps3 = record => {
    return {
      disabled: record.child === true, // 配置默认禁用的选项 含有孩子节点的字段名称为child =true
    };
  };
  /*
   * 单，多选类型时候使用
   * */
  onChange3 = (selectedRowKeys, selectedRows) => {
    console.log(selectedRowKeys);
    this.setState({
      selectedRowKeys: selectedRowKeys,
    });
  };

  deleteClick(id) {
    const { dispatch } = this.props;
    confirm({
      title: '您确定要删除该特征数据吗?',
      okText: '确定',
      okType: 'danger',
      cancelText: '取消',
      onOk() {
        console.log('OK');

        dispatch({
          type: 'feature/featureRemove',
          payload: { id: id },
        }).then(res => {
          if (res.code == 200) {
            console.log(res);
            this.search();
          } else {
            message.error(res.message);
          }
        });
      },
      onCancel() {
        console.log('Cancel');
      },
    });
  }
  reset = () => {
    this.setState(
      {
        name: '',
        description: '',
        createdTime: [],
        updatedTime: [],
        current: 1,
        size: 10,
        total: 0,
      },
      () => {
        this.search();
      },
    );
  };
  nameBlur = (record, e, key) => {
    e.persist();
    const { list, listOld, modifyText } = this.state;
    var lists = this.deepClone(listOld);
    const { dispatch } = this.props;
    // var listOrder = this.getPathByKey(record.id, 'id', list) || [];
    // console.log(listOrder);
    console.log(key);
    if (!modifyText || modifyText === listOld[key].name) {
      lists[key].editable = false;
      this.setState({
        list: lists,
        listOld: lists,
      });
      this.forceUpdate();
      return;
    }

    dispatch({
      type: 'feature/saveFusionFeature',
      payload: {
        id: record.id,
        name: e.target.value,
      },
    }).then(res => {
      if (res.code == 200) {
        // if (listOrder.length >= 1) {
        //   var text = 'lists[' + listOrder.join(']["children"][') + ']';
        //   eval(text).editable = false;
        //   eval(text).name = modifyText;
        // }
        //   eval(text).editable = false;
        //   eval(text).name = modifyText;
        lists[key].editable = false;
        lists[key].name = modifyText;
        this.setState({
          list: lists,
          listOld: lists,
        });
        this.forceUpdate();
      } else {
        // if (listOrder.length >= 1) {
        //   var text = 'list[' + listOrder.join(']["children"][') + ']';
        //   eval(text).errText = res.message ;
        // }
        list[key].errText = res.message;
        this.setState({
          list: list,
        });
        this.forceUpdate();
      }
    });
  };
  setListArr = (record, e, key) => {
    const { list, listOld } = this.state;
    var lists = this.deepClone(listOld);
    // var listOrder = this.getPathByKey(record.id, 'id', lists) || [];
    // console.log(listOrder);
    // if (listOrder.length >= 1) {
    //   var text = 'lists[' + listOrder.join(']["children"][') + ']';
    //   eval(text).editable = true;
    // }
    lists[key].editable = true;
    this.setState({
      list: lists,
      modifyText: record.name,
    });
    this.forceUpdate();
  };
  getPathByKey = (value, key, arr) => {
    let temppath = [];
    try {
      function getNodePath(node, index) {
        temppath.push(index);
        if (node[key] === value) {
          throw 'GOT IT!';
        }
        if (node.children && node.children.length > 0) {
          for (var i = 0; i < node.children.length; i++) {
            getNodePath(node.children[i], i);
          }
          temppath.pop();
        } else {
          temppath.pop();
        }
      }
      for (let i = 0; i < arr.length; i++) {
        getNodePath(arr[i], i);
      }
    } catch (e) {
      return temppath;
    }
  };
  // 定义一个深拷贝函数  接收目标target参数
  deepClone = target => {
    // 定义一个变量
    let result;
    // 如果当前需要深拷贝的是一个对象的话
    if (typeof target === 'object') {
      // 如果是一个数组的话
      if (Array.isArray(target)) {
        result = []; // 将result赋值为一个数组，并且执行遍历
        for (let i in target) {
          // 递归克隆数组中的每一项
          result.push(this.deepClone(target[i]));
        }
        // 判断如果当前的值是null的话；直接赋值为null
      } else if (target === null) {
        result = null;
        // 判断如果当前的值是一个RegExp对象的话，直接赋值
      } else if (target.constructor === RegExp) {
        result = target;
      } else {
        // 否则是普通对象，直接for in循环，递归赋值对象的所有值
        result = {};
        for (let i in target) {
          result[i] = this.deepClone(target[i]);
        }
      }
      // 如果不是对象的话，就是基本数据类型，那么直接赋值
    } else {
      result = target;
    }
    // 返回最终结果
    return result;
  };
  search = num => {
    const { dispatch } = this.props;
    const { name, description, createdTime, updatedTime, current, size } = this.state;
    const search = {
      name,
      description,
      createdTimeStart: createdTime[0] ? moment(createdTime[0]).format('YYYY-MM-DD h:mm:ss') : '',
      createdTimeEnd: createdTime[1] ? moment(createdTime[1]).format('YYYY-MM-DD h:mm:ss') : '',
      updatedTimeStart: updatedTime[0] ? moment(updatedTime[0]).format('YYYY-MM-DD h:mm:ss') : '',
      updatedTimeEnd: updatedTime[1] ? moment(updatedTime[1]).format('YYYY-MM-DD h:mm:ss') : '',
      current: num ? num : current,
      size,
    };

    // console.log(dataJson2);
    // console.log(dataJson2.data);
    // var data = dataJson2.data;
    // var records = data.records;
    // this.setState({
    //   list: records,
    //   listOld: records,
    //   current: data.current,
    //   size: data.size,
    //   total: data.total,
    // });

    dispatch({
      type: 'feature/selectFusionFeature',
      payload: search,
    }).then(res => {
      if (res.code == 200) {
        console.log(res);
        // message.error(res.msg);
        var data = res.data;
        var records = data.records;
        this.setState({
          list: records,
          listOld: records,
          current: data.current,
          size: data.size,
          total: data.total,
        });
        console.log(this.state.list);
      } else {
        // message.error(res.msg);
      }
    });
  };
  getStorageEngine = record => {
    const { getEnum } = this.state;
    const brr = getEnum.filter(item => {
      return item.value == record.storageEngine;
    });
    if (brr.length) {
      return brr[0].name;
    } else {
      return '- -';
    }
  };

  componentWillUnmount() {}

  render() {
    const {
      current,
      size,
      total,
      visible,
      loading,
      list,
      listFolder,
      getEnum,
      featureList,
      listFolderState,
      listFoldererr,
    } = this.state;
    const { onCancel, onCreate, form } = this.props;
    const { getFieldDecorator } = form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };

    const columns1 = [
      {
        title: '特征数据',
        dataIndex: 'name',
        align: 'left',
        width: 300,
        render: (text, record, key) =>
          record.editable ? (
            <div className={styles.iptSet}>
              <Input
                value={this.state.listFolderText1}
                onChange={e => {
                  this.setState({
                    listFolderText1: e.target.value,
                    listFoldererr: '',
                  });
                }}
              />
              <span>{listFoldererr}</span>
            </div>
          ) : (
            <span>{record.name}</span>
          ),
      },
      {
        title: '描述',
        dataIndex: 'description',
        align: 'center',
        width: 200,
        render: (text, record, key) =>
          record.editable ? (
            <div className={styles.iptSet}>
              <Input
                value={this.state.listFolderText2}
                onChange={e => {
                  this.setState({
                    listFolderText2: e.target.value,
                  });
                }}
              />
            </div>
          ) : (
            <span>{record.description}</span>
          ),
      },
      {
        title: '总占用存储',
        dataIndex: 'zzycc',
        align: 'center',
        width: 100,
        render: (text, record, key) =>
          record.zzycc ? <span>{record.zzycc}</span> : <span>--</span>,
      },
      {
        title: '创建时间',
        dataIndex: 'createdTime',
        align: 'center',
        width: 200,
      },
    ];
    const columns = [
      {
        title: '特征数据',
        dataIndex: 'name',
        align: 'left',
        width: 300,
        sorter: true,
        render: (text, record, key) =>
          record.editable ? (
            <div className={styles.iptSet}>
              <Input
                value={this.state.modifyText}
                ref={function(input) {
                  if (input != null) {
                    input.focus();
                  }
                }}
                onBlur={e => {
                  this.nameBlur(record, e, key);
                }}
                onChange={e => {
                  this.setState({
                    modifyText: e.target.value,
                  });
                }}
              />
              <span>{record.errText}</span>
            </div>
          ) : (
            <span
              onClick={() => {
                console.log(record);
              }}
            >
              {record.name}
            </span>
          ),
      },
      {
        title: '描述',
        dataIndex: 'description',
        align: 'center',
        width: 300,
      },
      {
        title: '存储引擎',
        dataIndex: 'storageEngine',
        align: 'center',
        width: 200,
        render: (text, record) => <span>{this.getStorageEngine(record)}</span>,
      },
      {
        title: '创建人',
        dataIndex: 'createdBy',
        align: 'center',
        width: 200,
      },
      {
        title: '创建时间',
        dataIndex: 'createdTime',
        align: 'center',
        width: 200,
      },
      {
        title: '更新时间',
        dataIndex: 'updatedTime',
        align: 'center',
        width: 200,
      },

      {
        title: '操作',
        key: 'action',
        render: (text, record, key) => (
          <span>
            <Button
              className={styles.tabBtn}
              type="link"
              onClick={() => {
                router.push('/featureCenterList?id=' + record.id);
              }}
              disabled={!(record.tables && record.tables.length)}
            >
              详情
            </Button>
            <Button
              className={styles.tabBtn}
              type="link"
              onClick={e => {
                e.persist();
                this.setListArr(record, e, key);
              }}
              disabled={!(record.tables && record.tables.length)}
            >
              重命名
            </Button>
            <Button
              className={styles.tabBtn}
              disabled={record.tables}
              type="link"
              onClick={() => this.showModalmobile(record.id)}
            >
              移动到
            </Button>
            <Button
              className={styles.tabBtn}
              type="link"
              onClick={() => this.deleteClick(record.id)}
              disabled={record.tables}
            >
              删除
            </Button>
          </span>
        ),
        align: 'center',
        width: 200,
      },
    ];
    const pagination = {
      current: current,
      pageSize: size,
      total: total,
      showQuickJumper: true,
      onChange: (date, dateString) => {
        console.log(date);
        this.setState(
          {
            current: date,
          },
          this.search(date),
        );
      },
    };
    const expandedRowRender = e => {
      console.log(e);
      return (
        <Table columns={columns} dataSource={e.tables} pagination={false} showHeader={false} />
      );
    };
    return (
      <div className={styles.bmq}>
        <div className={styles.header}>
          <div className={styles.headerL}>特征列表</div>
          <Button icon="plus" type="primary" onClick={this.showModal}>
            新增特征
          </Button>
        </div>
        <div className={searchCss.search}>
          <div className={searchCss.searchItem}>
            <span>文件名称：</span>
            <Input
              className={searchCss.serachIpt}
              value={this.state.name}
              onChange={e => this.setState({ name: e.target.value })}
              placeholder="请输入业务指标"
              suffix={<Icon type="search" />}
            />
          </div>
          <div className={searchCss.searchItem}>
            <span>描述：</span>
            <Input
              className={searchCss.serachIpt}
              placeholder="请输入描述"
              value={this.state.description}
              onChange={e => this.setState({ description: e.target.value })}
              suffix={<Icon type="search" />}
            />
          </div>
          <div className={searchCss.searchItem}>
            <span>创建时间：</span>
            <RangePicker
              className={styles.picker}
              value={this.state.createdTime}
              onChange={this.onChangeTime1}
            />
          </div>

          <div className={searchCss.searchItem}>
            <span>更新时间：</span>
            <RangePicker
              className={styles.picker}
              value={this.state.updatedTime}
              onChange={this.onChangeTime2}
            />
          </div>
          <div className={searchCss.searchItem}>
            <Button className={searchCss.r20} onClick={this.reset}>
              重置
            </Button>
            <Button type="primary" onClick={() => this.search()}>
              搜索
            </Button>
          </div>
        </div>
        <div className={styles.table}>
          <Table
            columns={columns}
            expandedRowRender={expandedRowRender}
            dataSource={list}
            rowKey="id"
            scroll={{ x: 1600 }}
            pagination={pagination}
          ></Table>
        </div>
        <Modal
          title="新增特征"
          visible={this.state.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
          footer={[
            <Button key="back" onClick={this.handleCancel}>
              取消
            </Button>,
            <Button key="submit" type="primary" loading={loading} onClick={this.handleOk}>
              下一步
            </Button>,
          ]}
        >
          <Form {...formItemLayout}>
            <Form.Item label="特征路径">
              {featureList.id ? (
                <span className={styles.pointer} onClick={this.showModal1}>
                  {featureList.name}
                </span>
              ) : (
                <Button className={styles.paddl} type="link" onClick={this.showModal1}>
                  选择路径
                </Button>
              )}
            </Form.Item>
            <Form.Item label="数据类型">
              {getFieldDecorator('select', {
                initialValue: getEnum.length ? getEnum[0].value : '',
              })(
                <Select>
                  {getEnum.length
                    ? getEnum.map(v => {
                        return <Option value={v.value}>{v.name}</Option>;
                      })
                    : null}
                </Select>,
              )}
            </Form.Item>
            <Form.Item label="数据路径">{getFieldDecorator('description2')(<Input />)}</Form.Item>
          </Form>
        </Modal>

        <Modal
          title="选择路径"
          visible={this.state.visible1}
          onOk={this.handleOk1}
          onCancel={this.handleCancel1}
          zIndex={9999}
          width={1000}
          footer={[
            <Button
              key="add1"
              style={{ float: 'left' }}
              disabled={listFolderState}
              onClick={this.addFolder}
            >
              新建文件夹
            </Button>,
            <Button key="back1" onClick={this.handleCancel1}>
              取消
            </Button>,
            <Button key="submit1" type="primary" loading={loading} onClick={this.handleOk1}>
              确定
            </Button>,
          ]}
        >
          {listFolder.length ? (
            <Table
              columns={columns1}
              dataSource={listFolder}
              rowKey={'id'}
              defaultExpandAllRows={true}
              pagination={false}
              rowSelection={{
                type: 'radio',
                onChange: this.onChange3,
                selectedRowKeys: this.state.selectedRowKeys,
              }}
              onRow={record => {
                return {
                  onClick: () => {
                    console.log(record);
                    console.log(record.id);
                    this.setState({
                      selectedRowKeys: [record.id],
                    });
                  }, // 点击行
                };
              }}
            ></Table>
          ) : (
            ''
          )}
        </Modal>
      </div>
    );
  }
}

export default FeatureCenter;
