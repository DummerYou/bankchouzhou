import React, {useEffect, useRef, useState} from "react";
import * as echarts from 'echarts';

const Accuracy =  (props) => {
    const main2 = useRef(null);
    let chartInstance = null;
    useEffect(() => {
        const { acceptList,acceptLabel } = props
        renderChart(acceptList,acceptLabel);
    },[props, renderChart]);

    let renderChart = (list,labell) => {
        console.log('list',list)
        const names = list.map((item)=>{
            return item.name
        })
        const myChart = echarts.getInstanceByDom(main2.current);
        if(myChart)
            chartInstance = myChart;
        else
            chartInstance = echarts.init(main2.current);
        chartInstance.setOption({
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'cross',
                    label: {
                        backgroundColor: '#6a7985'
                    }
                }
            },
            legend: {
                data: names,
                // selected: {
				//     '负样本洛伦兹曲线' : false,
				//     'KS曲线' : false,
				//     //不想显示的都设置成false
				//   }
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
            },
            xAxis: [
                {
                    type: 'category',
                    boundaryGap: false,
                    data: labell
                }
            ],
            yAxis: [
                {
                    type: 'value',
                    
                }
            ],
            series:list
        })
    };

    return(
        <div>
            <div style={{height: 400}} ref={main2}/>
        </div>
    );
};

export default Accuracy