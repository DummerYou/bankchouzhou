import React, { PureComponent, Fragment } from 'react';
import styles from './index.css';
import { Input, Button, Icon, DatePicker ,Form } from 'antd';
import { connect } from 'dva';
import moment from 'moment';


@connect(({ feature }) => ({
  feature,
}))
@Form.create()
class addItem1 extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  componentDidMount() {
    
  }
  handleChange = e => {
    console.log(e.target.value);
    sessionStorage.setItem('addItems1', e.target.value);
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
        if(values.input1){
          sessionStorage.setItem('addItems1', values.input1);
          this.props.setActiveKey();
        }
      }
    });
  };

  render() {
    const { form } = this.props;
    const { getFieldDecorator } = form;
    const formItemLayout = {
      layout: 'vertical',
    };
    
    
    return (
      <div className={styles.bmq + ' noForm'}>
        <h3>基本信息</h3>
        <Form  {...formItemLayout} onSubmit={this.handleSubmit}>
        <Form.Item label="应用名称">
          {getFieldDecorator('input1')(
            <Input onChange={this.handleChange}/>,
          )}
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit">保存</Button>
        </Form.Item>
      </Form>
      </div>
    );
  }
}

export default addItem1;