import React, { PureComponent, Fragment } from 'react';
import styles from './index.css';
import { Input, Button, Icon, Select,Form  } from 'antd';
import { connect } from 'dva';
import moment from 'moment';

const { Option } = Select;

let timeout;
let currentValue;






@connect(({ feature }) => ({
  feature,
}))
@Form.create()
class addItem1 extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      value: undefined,
    };
  }

  componentDidMount() {
    
    const { dispatch} = this.props;
    dispatch({
      type: 'feature/getForecastServer',
      payload: {
        page:1, 
        size:100
      },
    }).then(res => {
      if (res.code == 200) {
        // message.error(res.msg);
        console.log(res)
        this.setState({ data:res.data.data });
      } else {
        
      }
    });
    
  }


  handleChange = value => {
    console.log(value);
    sessionStorage.setItem('addItems2', JSON.stringify(value));
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
        if(values.input1){
          sessionStorage.setItem('addItems2', JSON.stringify(values.input1));
          this.props.setActiveKey();
        }
      }
    });
  };

  render() {
    const { form } = this.props;
    const { getFieldDecorator } = form;
    const {data} = this.state
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };
    //const options = this.state.data.map(d => <Option key={d.value}>{d.text}</Option>);

    return (
      <div className={styles.bmq}>
        <h3>选择模型预估应用</h3>
        <Form  {...formItemLayout} onSubmit={this.handleSubmit}>
        <Form.Item >
          {getFieldDecorator('input1')(
            <Select labelInValue onChange={this.handleChange}>
              {
                data.length?data.map((v)=>{
                  return <Option value={v.id} key={v.id}>{v.name}</Option>
                }):null
              }
          </Select>,
          )}
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit">保存</Button>
        </Form.Item>
      </Form>
        
      
      </div>
    );
  }
}

export default addItem1;