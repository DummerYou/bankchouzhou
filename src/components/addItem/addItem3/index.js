import React, { PureComponent, Fragment } from 'react';
import styles from './index.css';
import { Input, InputNumber, Button, Icon, DatePicker, message, Form, Radio, Select } from 'antd';
import { connect } from 'dva';
import router from 'umi/router';
import moment from 'moment';
const { Option } = Select;
const { MonthPicker, RangePicker, WeekPicker } = DatePicker;
const { TextArea } = Input;

@connect(({ feature }) => ({
  feature,
}))
@Form.create()
class addItem3 extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      dagData1: [],
      dagData2: [],
      checkbox2: '',
      selectAfterV: '1',
    };
  }

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'feature/getDag1',
      payload: {
        page: 1,
        size: 100,
      },
    }).then(res => {
      if (res.code == 200) {
        // message.error(res.msg);
        console.log(res);
        this.setState({ dagData1: res.data });
      } else {
      }
    });
    dispatch({
      type: 'feature/getDag2',
      payload: {
        page: 1,
        size: 100,
      },
    }).then(res => {
      if (res.code == 200) {
        // message.error(res.msg);
        console.log(res);
        this.setState({ dagData2: res.data });
      } else {
      }
    });
  }
  handleChange = value => {
    console.log(`selected ${value}`);
  };
  handleChange1 = value => {
    console.log(`selected ${value}`);
    this.setState({
      checkbox2: value,
    });
  };
  onChange = (date, dateString) => {
    console.log(date, dateString);
  };
  handleSubmit = e => {
    const { dispatch } = this.props;
    const { selectAfterV } = this.state;
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        var serverName = sessionStorage.getItem('addItems1');
        var timeInter = JSON.parse(sessionStorage.getItem('addItems2'));
        const datas = {
          // createdBy: values.effectivenessDag,
          serverName:serverName,
          effectivenessDag: values.effectivenessDag,
          stabilityDag: values.stabilityDag,
          targetServerId: timeInter.key,
          targetServerName: timeInter.label,
          timeInterval: values.timeInterval,
          timeOutSeconds: values.timeOutSeconds*60,
          timeUnit: selectAfterV*1,
        };
        
        console.log(datas)

        dispatch({
          type: 'feature/save',
          payload: datas,
        }).then(res => {
          console.log(res)
          if (res.code == 200) {
            // message.error(res.msg);
            message.success(res.message || "添加成功！");
            router.push('/');
          } else {
            message.error(res.message || "添加失败！");
          }
        });
      }
    });
  };
  checkPrice = (rule, value, callback) => {
    console.log(value);
    if (value > 0) {
      return callback();
    }
    callback('请输入数字!');
  };
  selectAfterC = key => {
    console.log(key);
    this.setState({
      selectAfterV: key,
    });
  };

  render() {
    const { form } = this.props;
    const { getFieldDecorator } = form;
    const { checkbox2, dagData1,dagData2, selectAfterV } = this.state;

    const formItemLayout = {
      layout: 'vertical',
    };
    const selectAfter = (
      <Select value={selectAfterV} style={{ width: 80 }} onChange={this.selectAfterC}>
        <Option value="1">小时</Option>
        <Option value="2">天</Option>
      </Select>
    );

    const selectAfter2 = (
      <Select value="分" style={{ width: 80 }}>
        <Option value="分">分</Option>
      </Select>
    );
    return (
      <div className={styles.bmq + ' noForm'}>
        <Form {...formItemLayout} className={' w80'} onSubmit={this.handleSubmit}>
          <Form.Item label="配置模式">
            <Radio.Group value="b">
              <Radio value="b">自定义dag</Radio>
            </Radio.Group>
          </Form.Item>
          <Form.Item label="模型稳定性监控">
            {getFieldDecorator('stabilityDag', {
              rules: [{ required: true, message: '请选择!' }],
            })(
              <Select onChange={this.handleChange}>
                {dagData1.length
                  ? dagData1.map(v => {
                      return (
                        <Option value={v.path} key={v.path}>
                          {v.name}
                        </Option>
                      );
                    })
                  : null}
              </Select>,
            )}
          </Form.Item>

          <Form.Item label="模型有效性和模型变量监控">
            {getFieldDecorator('effectivenessDag', {
              rules: [{ required: true, message: '请选择!' }],
            })(
              <Select onChange={this.handleChange}>
                {dagData2.length
                  ? dagData2.map(v => {
                      return (
                        <Option value={v.path} key={v.path}>
                          {v.name}
                        </Option>
                      );
                    })
                  : null}
              </Select>,
            )}
          </Form.Item>

          <Form.Item label="监控运行策略"></Form.Item>
          <Form.Item label="运行模式">
            <Radio.Group value="b">
              <Radio value="b">循环运行</Radio>
            </Radio.Group>
          </Form.Item>
          <Form.Item label="循环周期">
            {getFieldDecorator('timeInterval', {
              rules: [{ validator: this.checkPrice }],
            })(
              //  <InputNumber min={0}  precision={0} addonAfter="天前" defaultValue="mysite" />,
              <Input addonAfter={selectAfter} />,
            )}
          </Form.Item>
          <Form.Item label="任务超时时长">
            {getFieldDecorator('timeOutSeconds', {
              rules: [{ validator: this.checkPrice }],
            })(
              //  <InputNumber min={0}  precision={0} addonAfter="天前" defaultValue="mysite" />,
              <Input addonAfter={selectAfter2} />,
            )}
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit">
              保存
            </Button>
          </Form.Item>
        </Form>
      </div>
    );
  }
}

export default addItem3;
