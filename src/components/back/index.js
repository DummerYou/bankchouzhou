import React, { useEffect, useRef, useState } from 'react';
import * as echarts from 'echarts';

const Back = props => {
  const main2 = useRef(null);
  let chartInstance = null;
  useEffect(() => {
    const { list } = props;
    renderChart(list);
  }, [props, renderChart]);

  // let array = [];
  let renderChart = list => {
    const name = list.map(item => {
      return item.name;
    });
    const myChart = echarts.getInstanceByDom(main2.current);
    if (myChart) chartInstance = myChart;
    else chartInstance = echarts.init(main2.current);
    chartInstance.setOption({
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          // 坐标轴指示器，坐标轴触发有效
          type: 'shadow', // 默认为直线，可选为：'line' | 'shadow'
        },
      },
      legend: {
        data: name,
      },
      grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true,
      },
      xAxis: [
        {
          type: 'value',
        },
      ],
      yAxis: [
        {
          type: 'category',
          axisTick: {
            show: false,
          },
          data: ['y_mean', 'prediction_mean'],
        },
      ],
      series: list,
    });
  };
  return (
    <div>
      <div style={{ height: 400 }} ref={main2} />
    </div>
  );
};

export default Back;
