import React, { useState, useEffect } from 'react';
import { Button, Table, Tabs, Divider } from 'antd';
import Back from '../back/index';
import FeatureModel from '../model/index';
import styles from '../moreClassify/index.css';
const { TabPane } = Tabs;

const Flyback = (props) => {
  const [dataList, setList] = useState([]);
  const [list, setBackList] = useState([]);
  const [model, setModelList] = useState([]);

  let name = ""
  try {
    name = " _ " + props.location.query.name
  } catch (error) {
    
  }
  if(document.getElementById('headerName')){
    document.getElementById('headerName').innerHTML = ' > 模型迭代对比' + name;
  }
  useEffect(() => {
    //请求接口
    (async () => {
      const { state } = props.location;
      let states = state
      if(!states){
        states = {
          featureScoreList:JSON.parse(sessionStorage.getItem('featureScoreList')) || [],
          featureMetricList:JSON.parse(sessionStorage.getItem('featureMetricList')) || [],
        }
      }
      const { featureScoreList,featureMetricList } = states || {};
      sessionStorage.setItem('featureScoreList', JSON.stringify(featureScoreList));
      sessionStorage.setItem('featureMetricList', JSON.stringify(featureMetricList));
      const list = featureScoreList.map(item => {
        return {
          name: item.model,
          type: 'bar',
          emphasis: {
            focus: 'series',
          },
          data: [item.y_mean, item.prediction_mean],
        };
      });
      setList(featureScoreList);
      setBackList(list);
      setModelList(featureMetricList)
    })();
  }, [props.location]);
  const toPercent = point => {
    if (point == 0) {
      return 0;
    }
    let str = Number(point).toFixed(2);
    return str;
  };
  const columns = [
    {
      title: '模型',
      dataIndex: 'model',
      key: 'model',
    },
    {
      title: 'mse',
      dataIndex: 'mse',
      key: 'mse',
      render: (text, record) => toPercent(text),
    },
    {
      title: 'mae',
      dataIndex: 'mae',
      key: 'mae',
      render: (text, record) => toPercent(text),
    },
    {
      title: 'prediction_mean',
      dataIndex: 'prediction_mean',
      key: 'prediction_mean',
      render: (text, record) => toPercent(text),
    },
    {
      title: 'y_mean',
      dataIndex: 'y_mean',
      key: 'y_mean',
      render: (text, record) => toPercent(text),
    },
  ];

  return (
    <>
      <div className={styles.table}>
        <span className={styles.txt}>模型指标</span>
        <Table dataSource={dataList} bordered columns={columns} pagination={false} />
        <div className={styles.tabs}>
          <Back list={list} />
        </div>
        <div className={styles.count}>
          <span className={styles.txt}>特征重要性（模型）</span>
          <div className={styles.model}>
            <Tabs defaultActiveKey="1" tabPosition="left">
              {model.map((item, index) => {
                return (
                  <TabPane tab={item.model} key={index}>
                    <FeatureModel modelData={item.featureDetails} />
                  </TabPane>
                );
              })}
            </Tabs>
          </div>
        </div>
      </div>
    </>
  );
};
export default Flyback;
