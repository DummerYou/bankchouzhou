import React, {useEffect, useRef, useState} from "react";
import * as echarts from 'echarts';

const Indicators =  (props) => {
    const main2 = useRef(null);
    let chartInstance = null;
    useEffect(() => {
        const { reportList } = props
        renderChart(reportList);
    },[props, renderChart]);

    let renderChart = (list) => {
       const names =  list.map((item)=>{
            return item.name;
        })
        console.log('list',list)
       const datas = list.map((item)=>{
           return item.data
       })
        const myChart = echarts.getInstanceByDom(main2.current);
        if(myChart)
            chartInstance = myChart;
        else
            chartInstance = echarts.init(main2.current);
        chartInstance.setOption({
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'shadow'
                }
            },
            xAxis: {
                type: 'value',
            },
            yAxis: {
                data: names,
                type: 'category'
                
            },
            series: [{
                data:datas,
                type: 'bar',
                barMinHeight:30,  //最小柱高
                barWidth: 40,  //柱宽度
                barMaxWidth:100,   //最大柱宽度
            }]
        })
    };
    return(
        <div>
            <div style={{height: 400}} ref={main2}/>
        </div>
    );
};

export default Indicators