import React, {useEffect, useRef, useState} from "react";
import * as echarts from 'echarts';

const KS =  (props) => {
    const main2 = useRef(null);
    let chartInstance = null;
    useEffect(() => {
        const { rocList,xks } = props
        renderChart(rocList,xks);
    },[props, renderChart]);

    let renderChart = (list,xks) => {
        const myChart = echarts.getInstanceByDom(main2.current);
        if(myChart)
            chartInstance = myChart;
        else
            chartInstance = echarts.init(main2.current);
        chartInstance.setOption({
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'cross',
                    label: {
                        backgroundColor: '#6a7985'
                    }
                }
            },
            legend: {
                data: ['正样本洛伦兹曲线', '负样本洛伦兹曲线','KS曲线'],
                selected: {
				    '负样本洛伦兹曲线' : false,
				    'KS曲线' : false,
				    //不想显示的都设置成false
				  }
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
            },
            xAxis: [
                {
                    type: 'category',
                    boundaryGap: false,
                    data: xks
                }
            ],
            yAxis: [
                {
                    type: 'value',
                    
                }
            ],
            series:list
        })
    };

    return(
        <div>
            <div style={{height: 400}} ref={main2}/>
        </div>
    );
};

export default KS