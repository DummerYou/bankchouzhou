import { xAxisStyle, yAxisStyle } from '../axis';
export const getBinaryStatisticsItemStyle = (color = '#91CCBC') => ({
    normal: {
        color,
    },
});
export const commonCompareReportColorList = ['#91CCBC', '#95C0FC', '#F0A8A9', '#F1C298', '#A5A6E3', '#D9A5DE'];
export const isDisplayBinaryStatisticsLabel = reportNum => ({
    normal: {
        show: !(reportNum > 4),
        position: 'top',
        textStyle: {
            color: '#000000',
        },
    },
});
export const binaryStatisticsSeriesItem = {
    type: 'bar',
    barMaxWidth: 50,
    animation: false,
    label: isDisplayBinaryStatisticsLabel(),
    itemStyle: getBinaryStatisticsItemStyle(),
    data: [],
};
export default {
    tooltip: {
        show: true,
    },
    grid: {
        x: 40, // 默认是80px
        y: 20, // 默认是60px
        x2: 20, // 默认80px
        y2: 80, // 默认60px
    },
    xAxis: {
        type: 'category',
        ...xAxisStyle,
        data: [
            '召回率1(Recall)',
            'TNR(Specificity)',
            '精准率(Precision)',
            'NPV',
            '准确率(Accuracy)',
            'F1 score',
        ],
        axisLabel: {
            rotate: 30,
            interval: 0,
            margin: 12,
            fontSize: 12,
            color: '#626262',
        },
    },
    yAxis: {
        type: 'value',
        ...yAxisStyle,
        axisLabel: {
            formatter(value) {
                if (value !== 0) return `${value * 100}%`;
                return value;
            },
            color: '#626262',
        },
        min: 0,
        max: 1,
    },
    series: [binaryStatisticsSeriesItem],
};