import React, { useEffect, useRef, useState } from 'react';
import { Button, Table, Input, Tabs, Select, Divider, Form } from 'antd';
import { SearchOutlined } from '@ant-design/icons';
import styles from './index.css';

const FeatureModel = props => {
  const { modelData } = props
  const { Option } = Select;
  const [query, setQuery] = useState();

  const columns = [
    {
      title: '变量名称',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: '重要性',
      dataIndex: 'importance',
      key: 'importance',
      render: (text, record) => (
        <div className={styles.font}>
          <div className={styles.test} style={{ width:text*1,maxWidth:100,minWidth:0.001 }}></div>
          {text}
        </div>
      ),
    },
    {
      title: '变量类型',
      dataIndex: 'type',
      key: 'type',
    },
  ];
  return (
    <div>
      {/* <div className={styles.title}>
        <div className={styles.left}>
          <Form
            name="basic"
            labelCol={{ span: 8 }}
            wrapperCol={{ span: 1 }}
            style={{ display: 'flex' }}
            // initialValues={{ remember: true }}
            //   onFinish={onFinish}
            //   onFinishFailed={onFinishFailed}
          >
            <Form.Item label="筛选指标" name="筛选指标">
              <Select defaultValue="重要性" style={{ width: 120 }} bordered={false}>
                <Option value="重要性">重要性</Option>
                <Option value="一般重要">一般重要</Option>
              </Select>
            </Form.Item>
            <Form.Item>
              <Button htmlType="button">清空</Button>
              <Button type="primary" htmlType="submit">
                筛选
              </Button>
            </Form.Item>
          </Form>
        </div>
        <Input style={{ width: '180px' }} prefix={<SearchOutlined />} />
      </div> */}
      <Table dataSource={modelData} bordered columns={columns} pagination={false} />
    </div>
  );
};
export default FeatureModel;
