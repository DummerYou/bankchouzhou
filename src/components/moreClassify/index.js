import React, { useState, useEffect } from 'react';
import { Button, Table, Tabs, Divider } from 'antd';
import Indicators from '../indicators/index'
import Accuracy from '../accuracy/index'
import Report from '../report/index'
import styles from './index.css';
const { TabPane } = Tabs;

const SecondCategory = (props) => {
  const [dataList, setList] = useState([]);
  const [reportList, setReportList] = useState([]);
  const [acceptList, setAcceptList] = useState([]);
  const [acceptLabel, setAcceptX] = useState([]);
  const [model, setModel] = useState([]);
  let name = ""
  try {
    name = " _ " + props.location.query.name
  } catch (error) {
    
  }
  if(document.getElementById('headerName')){
    document.getElementById('headerName').innerHTML = ' > 模型迭代对比' + name;
  }
  useEffect(() => {
    //请求接口
    (async()=>{
        const { state } = props.location;
        let states = state
        if(!states){
          states = {
            featureStatisticList:JSON.parse(sessionStorage.getItem('featureStatisticList')) || [],
            featureScoreList:JSON.parse(sessionStorage.getItem('featureScoreList')) || [],
          }
        }
        const { featureStatisticList = [],featureScoreList=[] } = states || {}
        sessionStorage.setItem('featureStatisticList', JSON.stringify(featureStatisticList));
        sessionStorage.setItem('featureScoreList', JSON.stringify(featureScoreList));
        if(featureScoreList.length ===0) return
        const list =  featureStatisticList.length > 0 &&  featureStatisticList.map((item)=>{
            return {
                model:item.model,
                classify:item.detailList[0] && [item.detailList[0].count,item.detailList[0].proportion],
                classify2:item.detailList[1] && [item.detailList[1].count,item.detailList[1].proportion],
                classify3:item.detailList[2] && [item.detailList[2].count,item.detailList[2].proportion],
            }
        })
        const report =  featureScoreList.length > 0 &&  featureScoreList.map((item)=>{
            return {                
                    name: item.model,
                    type: 'bar',
                    data: item.accuracy
            }
        })

        const acceptX = featureScoreList[1].effectiveLabels;
        setAcceptX(acceptX);
        const accepts = featureScoreList.map(item => {
          return {
              name: item.model,
              type: 'line',
              smooth: true,
              data: item.topN,
            }
          ;
        });
        setList(list)
        setReportList(report)
        setAcceptList(accepts)
        setModel(featureScoreList)
    })();
  }, [props.location]);

  const toPercent = point => {
    if (point == 0) {
      return 0;
    }
    let str = Number(point).toFixed(2);
    str += '%';
    return str;
  };
  const columns = [
    {
      title: '模型',
      dataIndex: 'model',
      key: 'model',
    },
    {
      title: '类别1样本数／占比',
      dataIndex: 'classify',
      key: 'classify',
      render: (text, record) => text && (text[0] +"／"+ toPercent(text[1])) ,

    },
    {
      title: '类别2样本数／占比',
      dataIndex: 'classify2',
      key: 'classify2',
      render: (text, record) => text && (text[0] +"／"+ toPercent(text[1])) ,
    },
    {
        title: '类别3样本数／占比',
        dataIndex: 'classify3',
        key: 'classify3',
        render: (text, record) => text && (text[0] +"／"+ toPercent(text[1])) ,
    },
  ];

  return (
    <>
      <div className={styles.table}>
        <span className={styles.txt}>特征统计</span>
        <Table dataSource={dataList} bordered columns={columns} pagination={false} />
        <div className={styles.tabs}>
            <span className={styles.txt}>模型指标</span>
            <Indicators reportList={reportList}/>
            <span className={styles.txt}>TopN准确率曲线</span>
            <Accuracy  acceptList={acceptList} acceptLabel={acceptLabel}/>
            <div className={styles.count}>
          <span className={styles.txt}>报告</span>
          <div className={styles.model}>
            <Tabs defaultActiveKey="1" tabPosition="left">
              {model.map((item, index) => {
                return (
                  <TabPane tab={item.model} key={index}>
                    <Report modelData={item.matrixNum} per={item.matrixPer}/>
                  </TabPane>
                );
              })}
            </Tabs>
          </div>
        </div>
        </div>
      </div>
    </>
  );
}
export default SecondCategory;
