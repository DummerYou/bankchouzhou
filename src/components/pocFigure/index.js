import React, {useEffect, useRef, useState} from "react";
import * as echarts from 'echarts';

const PocFigure=  (props) => {
    const main2 = useRef(null);
    let chartInstance = null;
    useEffect(() => {
        const { rocList,roc } = props
        renderChart(rocList,roc);
    },[props, renderChart]);

    let renderChart = (list,roc) => {
        const myChart = echarts.getInstanceByDom(main2.current);
        if(myChart)
            chartInstance = myChart;
        else
            chartInstance = echarts.init(main2.current);
        chartInstance.setOption({
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'cross',
                    label: {
                        backgroundColor: '#6a7985'
                    }
                }
            },
            legend: {
                data: ['评估报告1', '评估报告2']
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
            },
            xAxis: [
                {
                    type: 'category',
                    data: roc,
                }
            ],
            yAxis: [
                {
                    type: 'value',
                }
            ],
            series: list
        })
    };

    return(
        <div>
            <div style={{height: 400}} ref={main2}/>
        </div>
    );
};

export default PocFigure