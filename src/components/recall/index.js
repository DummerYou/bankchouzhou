import React, { useEffect, useRef, useState } from 'react';
import * as echarts from 'echarts';

const Recall = props => {
  const main2 = useRef(null);
  let chartInstance = null;
  useEffect(() => {
    const { rocList, recall } = props;
    renderChart(rocList, recall);
  }, [props, renderChart]);

  let renderChart = (list, recall) => {
    const myChart = echarts.getInstanceByDom(main2.current);
    if (myChart) chartInstance = myChart;
    else chartInstance = echarts.init(main2.current);
    chartInstance.setOption({
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'cross',
        },
      },
      xAxis: {
        type: 'category',
        data: recall,
      },
      yAxis: {
        type: 'value',
      },
      series: list,
    });
  };

  return (
    <div>
      <div style={{ height: 400 }} ref={main2} />
    </div>
  );
};

export default Recall;
