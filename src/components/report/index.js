import React, { useEffect, useRef, useState } from 'react';
import * as echarts from 'echarts';
import styles from './index.css';
const Report = props => {
  const [tableModel, SetTable] = useState([]);
  const [preModel, SetPreModel] = useState([]);

  useEffect(() => {
    const { modelData, per } = props;
    SetTable(modelData);
    SetPreModel(per);
  }, [props]);

  const toFixed = (number, decimal = 4) => {
    if (typeof number === 'number') {
      return parseFloat(number.toFixed(decimal));
    }
    return '';
  };

  const renderMatrix = () => {
    const matrixNum = tableModel;
    const matrixPer = preModel;
    const matrix = [];
    const matrixTitle = [];
    matrixTitle.push(<td />);
    const maxLength = matrixNum.length < 20 ? matrixNum.length : 20;
    for (let x = 0; x < maxLength; x += 1) {
      matrixTitle.push(<td className={styles.visualization_matrix_colNum}>{x + 1}</td>);
    }
    matrix.push(<tr className={styles.visualization_matrix_colNum_tr}>{matrixTitle}</tr>);
    for (let i = 0; i < maxLength; i += 1) {
      const matrixLine = [];
      matrixLine.push(<td className={styles.visualization_matrix_lineNum}>{i + 1}</td>);
      const maxColLength = matrixNum[i].length < 20 ? matrixNum[i].length : 20;
      for (let j = 0; j < maxColLength; j += 1) {
        if (matrixPer[i][j] || matrixPer[i][j] ===0) {
          const colorNum = Math.ceil(matrixPer[i][j] * 10);
          if (i === j) {
            // const classString = `styles.`;
            matrixLine.push(
              <td className={styles.visualization_matrix_td}>
                <div className={styles.visualization_matrix_colorE2}>
                  <span className="big-text">{toFixed(matrixPer[i][j] * 100, 2)}%</span>
                  <span>{matrixNum[i][j]}</span>
                </div>
              </td>,
            );
          } else {
            // const classString = `visualization_matrix_td visualization_matrix_colorN${colorNum}`;
            matrixLine.push(
              <td className={styles.visualization_matrix_td}>
                <div className={styles.visualization_matrix_colorE5}>
                  <span
                    style={{
                      fontSize: '18px',
                      letterSpacing: 0,
                      color: '#333333',
                      lineHeight: 1.5,
                    }}
                  >
                    {toFixed(matrixPer[i][j] * 100, 2)}%
                  </span>
                  <span>{matrixNum[i][j]}</span>
                </div>
              </td>,
            );
          }
        } else {
          matrixLine.push(<td className="visualization_matrix_td" />);
        }
      }
      matrix.push(<tr>{matrixLine}</tr>);
    }
    return matrix;
  };

  return (
    <div className={styles.visualization_matrix_div}>
      <table className={styles.visualization_matrix_table}>
        <tbody>{renderMatrix()}</tbody>
      </table>
    </div>
  );
};
export default Report;
