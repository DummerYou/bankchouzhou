import React, { useState, useEffect } from 'react';
import { Button, Table, Tabs, Divider } from 'antd';
import PocFigure from '../pocFigure/index';
import Recall from '../recall/index';
import Left from '../left/index';
import KS from '../ks/index';
import FeatureModel from '../model/index';
import Threshold from '../threshold/index'
import styles from './index.css';
const { TabPane } = Tabs;

const SecondCategory = (props) => {
  // console.log('props',props)
  const [dataList, setList] = useState([]);
  const [rocList, setRocList] = useState([]);
  const [recallList, setRecallList] = useState([]);
  const [leftList, setLeftList] = useState([]);
  const [ksList, setKsList] = useState([]);
  const [gainList, setGainList] = useState([]);
  const [model, setModelList] = useState([]);
  const [xlist, setXList] = useState([]);
  const [xrecall, setXRecall] = useState([]);
  const [xleft, setXLeft] = useState([]);
  const [xks, setXks] = useState([]);
  const [gainX, setgainX] = useState([]);
  const [should, setShouldList] = useState([]);
  const [featureLists, setFeatureLists] = useState([]);
  let name = ""
  try {
    name = " _ " + props.location.query.name
  } catch (error) {
    
  }
  if(document.getElementById('headerName')){
    document.getElementById('headerName').innerHTML = ' > 模型迭代对比' + name;
  }
  useEffect(() => {
    //请求接口
    (async () => {
      //await
      const { state } = props.location;
      let states = state
      if(!states){
        states = {
          featureStatisticList:JSON.parse(sessionStorage.getItem('featureStatisticList')) || [],
          featureScoreList:JSON.parse(sessionStorage.getItem('featureScoreList')) || [],
          featureMetricList:JSON.parse(sessionStorage.getItem('featureMetricList')) || [],
        }
      }
      const { featureStatisticList, featureScoreList, featureMetricList } = states;
      sessionStorage.setItem('featureStatisticList', JSON.stringify(featureStatisticList));
      sessionStorage.setItem('featureScoreList', JSON.stringify(featureScoreList));
      sessionStorage.setItem('featureMetricList', JSON.stringify(featureMetricList));
      if (featureScoreList.length ===0)  return
      const rocX = featureScoreList[1].roc.xlist;
      setXList(rocX);
      let featureListss = []
      const list = featureScoreList.map((item,index) => {
        featureListss[index] = {
          auc:item.auc,
          model:item.model,
        }
        return {
          name: item.model,
          type: 'line',
          areaStyle: {},
          smooth: true,
          emphasis: {
            focus: 'series',
          },
          symbol: 'none',
          data: item.roc.ylist,
        };
      });
      console.log(featureListss);

      const recallX = featureScoreList[1].recall.xlist;
      setXRecall(recallX);
      const callList = featureScoreList.map(item => {
        return {
          name: item.model,
          data: item.recall.ylist,
          type: 'line',
          symbol: 'none',
          smooth: true,
        };
      });
      const leftX = featureScoreList[1].lift.xlist;
      setXLeft(leftX);
      const Left = featureScoreList.map(item => {
        return {
          name: item.model,
          data: item.lift.ylist,
          type: 'line',
          symbol: 'none',
          smooth: true,
        };
      });
      const ksX = featureScoreList[1].ks.cumulativeGood.xlist;
      setXks(ksX);
      const ks = featureScoreList.map(item => {
        return [
          {
            name: '负样本洛伦兹曲线',
            type: 'line',
            smooth: true,
            symbol: 'none',
            itemStyle: {
              normal: {
                lineStyle: {
                  width:4,
                  type: item.model === 'model_3' ? 'dotted' : 'solid',
                },
              },
            },
            data: item.ks.cumulativeGood.ylist,
          },
          {
            name: '正样本洛伦兹曲线',
            type: 'line',
            smooth: true,
            symbol: 'none',
            itemStyle: {
                normal: {
                  lineStyle: {
                    width:4,
                    type: item.model === 'model_3' ? 'dotted' : 'solid',
                  },
                },
              },
            data: item.ks.cumulativeBad.ylist,
          },
          {
            name: 'KS曲线',
            type: 'line',
            smooth: true,
            symbol: 'none',
            itemStyle: {
                normal: {
                  lineStyle: {
                    width:4,
                    type: item.model === 'model_3' ? 'dotted' : 'solid',
                  },
                },
              },
            data: item.ks.cumulativeKSDiff.ylist,
          },
        ];
      });
      const ksLists = [...ks[0], ...ks[1]];

      const GAX = featureScoreList[1].gain.xlist;
      setgainX(GAX);
      const gain = featureScoreList.map(item => {
        return {
          name: item.model,
          data: item.gain.ylist,
          type: 'line',
          symbol: 'none',
          smooth: true,
        };
      });
      const shoulds = featureScoreList.map((item)=>{
        return {
          recallByThresholds:item.confusionMetrics.recallByThresholds,
          specificityByThresholds:item.confusionMetrics.specificityByThresholds,
          precisionByThresholds:item.confusionMetrics.precisionByThresholds,
          npvByThresholds:item.confusionMetrics.npvByThresholds,
          accuracyByThresholds:item.confusionMetrics.accuracyByThresholds,
          f1ScoreByThresholds:item.confusionMetrics.f1ScoreByThresholds,
        }
      })
      setFeatureLists(featureListss);
      setList(featureStatisticList);
      setRocList(list);
      setRecallList(callList);
      setLeftList(Left);
      setKsList(ksLists);
      setGainList(gain);
      setModelList(featureMetricList);
      setShouldList(shoulds)
    })();
  }, [props]);

  const toPercent = (point) => {
    if (point == 0) {
      return 0;
    }
    let str = Number(point).toFixed(2);
    str += '%';
    return str;
  };

  const columns = [
    {
      title: '模型',
      dataIndex: 'model',
      key: 'model',
    },
    {
      title: '样本数',
      dataIndex: 'totalCount',
      key: 'totalCount',
    },
    {
      title: '正样本率',
      dataIndex: 'positiveSampleProportion',
      key: 'positiveSampleProportion',
      render: (text, record) => toPercent(text),
    },
    {
      title: '负样本率',
      dataIndex: 'negativeSampleProportion',
      key: 'negativeSampleProportion',
      render: (text, record) => toPercent(text),
    },
    {
      title: '特征组数',
      dataIndex: 'featureCount',
      key: 'featureCount',
    },
    {
      title: '特征维度',
      dataIndex: 'featureDimension',
      key: 'featureDimension',
    },
  ];
  const featureListsDiv = (e) => {
    console.log(e);
    return (
      <div className={styles.featureListsDiv}>
        {
          e.length?e.map((v)=>{
            return <div className={styles.featureListsSpan}>{v.model + "   Area Under the Curve = " + Number(v.auc).toFixed(3)}</div>
          }):null
        }
      </div>
    );
  };

  return (
    <>
      <div className={styles.table}>
        <span className={styles.txt}>特征统计</span>
        <Table className={styles.txt2} dataSource={dataList} bordered columns={columns} pagination={false} />
        <span className={styles.txt}>模型指标</span>
        <div className={styles.tabs + " " + styles.txt1}>
          <Tabs defaultActiveKey="1">
            <TabPane tab="ROC图" key="1">
              {featureListsDiv(featureLists)}
              <PocFigure roc={xlist} rocList={rocList} />
            </TabPane>
            <TabPane tab="Precision/Recall图" key="2">
              {featureListsDiv(featureLists)}
              <Recall recall={xrecall} rocList={recallList} />
            </TabPane>
            <TabPane tab="Lift图" key="3">
              {featureListsDiv(featureLists)}
              <Left left={xleft} rocList={leftList} />
            </TabPane>
            <TabPane tab="K-S图" key="4">
              {featureListsDiv(featureLists)}
              <KS rocList={ksList} xks={xks} />
            </TabPane>
            <TabPane tab="Gain图" key="5">
              {featureListsDiv(featureLists)}
              <Left rocList={gainList} left={gainX} />
            </TabPane>
          </Tabs>
        </div>
        <div className={styles.tabs}>
          <Threshold report={should}/>
        </div>
        <div className={styles.count}>
          <span className={styles.txt}>特征重要性（模型）</span>
          <div className={styles.model}>
            <Tabs defaultActiveKey="1" tabPosition="left">
              {model.map((item, index) => {
                return (
                  <TabPane tab={item.model} key={index}>
                    <FeatureModel modelData={item.featureDetails} />
                  </TabPane>
                );
              })}
            </Tabs>
          </div>
        </div>
      </div>
    </>
  );
};
export default SecondCategory;
