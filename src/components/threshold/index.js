import React, { useState, useEffect, useRef } from 'react';
import Slider from './metrics-slider';
import * as echarts from 'echarts';
import styles from './index.css';

const Threshold = props => {
  const main2 = useRef(null);
  let chartInstance = null;
  const [threshold, setThreshold] = useState(0.5);

  useEffect(() => {
    const { report} = props;
    getReportOptionConfig(report,threshold);
  }, [props, main2, threshold, getReportOptionConfig]);
  const getReportOptionConfig = (report,threshold) => {
    const index = Math.round(threshold * 100);
    const list =  report.map((item)=>{
      return[
        item.accuracyByThresholds[index],
        item.f1ScoreByThresholds[index],
        item.npvByThresholds[index],
        item.precisionByThresholds[index],
        item.recallByThresholds[index],
        item.specificityByThresholds[index],
      ]
    })
   const reports = list.length > 0  && list.map((item)=>{
     return {
      type: 'bar',
      data:item
     }
   })
   renderChart(reports)
  }
  let renderChart = (list) => {    
    const myChart = echarts.getInstanceByDom(main2.current);
    if (myChart) chartInstance = myChart;
    else chartInstance = echarts.init(main2.current);
    chartInstance.setOption({
      xAxis: {
        type: 'category',
        data: ['召回率(Recall)', 'TNR(sensitivity)', '精准率(precision)', 'NPV', '准确率(Accuracy)', 'F1(score)'],
      },
      yAxis: {
        type: 'value',
      },
      series: list
    });
  };

  const handleSliderChange = (threshold) => {
    if (!main2) return
    setThreshold(threshold);
  }
  return (
    <>
      <div className={styles.table}>
        <Slider  onSliderChange={handleSliderChange} />
        <div style={{ height: 400 }} ref={main2} />
      </div>
    </>
  );
};
export default Threshold;
