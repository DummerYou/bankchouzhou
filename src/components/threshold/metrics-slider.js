import React, { useState, useEffect } from 'react';
import { InputNumber, Slider as AntdSlider } from 'antd';
import styles from './index.css';

const Slider = (props) => {
  const [value, setValue] = useState(0.5);
  useEffect(() => {
    //请求接口
    (async () => {})();
  }, []);
  const onChange = value => {
    if (isNaN(value)) {
      return;
    }
    setValue(value);
    // this.props.onSliderChange(value);
    props.onSliderChange(value)
  };

  return (
    <>
      <div className={styles.center}>
        <div className={styles.list}>正负例判定阈值</div>
        <AntdSlider
          className={styles.silder}
          min={0}
          max={1}
          step={0.01}
          value={value}
          onChange={value => onChange(value)}
        />
        <InputNumber
          className={styles.number}
          min={0}
          max={1}
          step={0.01}
          value={value}
          onChange={value => onChange(value)}
        />
      </div>
    </>
  );
};
export default Slider;
