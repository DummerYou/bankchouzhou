import styles from './index.css';
import Link from 'umi/link';
import zhCN from 'antd/es/locale/zh_CN';
import router from 'umi/router';
import { ConfigProvider , Menu, Dropdown, Icon } from 'antd';
import { mobileValidator, setStorage, getStorage, rmStorage, decrypt,setCook,getCook } from '../services/utils';
const menu = (
  <Menu>
    <Menu.Item>
      <a rel="noopener noreferrer" href="/">
        退出登陆
      </a>
    </Menu.Item>
  </Menu>
);
function BasicLayout(props) {
  console.log(props);
  if(document.getElementById('headerName') && props.location.pathname == "/"){
    document.getElementById('headerName').innerHTML = '';
  }
  return (
    <div className={styles.normal}>
      <div className={styles.header}>
        <div><span  className={styles.logo} onClick={()=>{router.push('/');}}>应用列表</span><span id="headerName"></span></div>
        <div className={styles.userInfo}>
          
        </div>
      </div>
      <ConfigProvider locale={zhCN}>
      <div className={styles.conten}>{props.children}</div>
      </ConfigProvider>
    </div>
  );
}

export default BasicLayout;
