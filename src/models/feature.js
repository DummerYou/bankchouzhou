import { login,serverDelete,modelDetails,query,getDag,getDag1,getDag2,realTimePredictor,predictor,getForecastServer,save,history,validityHistogram,batchPredictor,validityQuery,points,lineChart,psiReport,effectiveness,stability} from '../services/api';

export default {
    namespace: 'feature',
    state: {
        data: {

        }
    },
    effects: {
        
        *login ({ payload }, { call, put }) {
            const response = yield call(login, payload);
            return response;
        },
        
        
        *serverDelete ({ payload }, { call, put }) {
            const response = yield call(serverDelete, payload);
            return response;
        },
        *query ({ payload }, { call, put }) {
            const response = yield call(query, payload);
            return response;
        },
        
        *getDag ({ payload }, { call, put }) {
            const response = yield call(getDag, payload);
            return response;
        },
        *getDag1 ({ payload }, { call, put }) {
            const response = yield call(getDag1, payload);
            return response;
        },
        *getDag2 ({ payload }, { call, put }) {
            const response = yield call(getDag2, payload);
            return response;
        },
        
        *getForecastServer ({ payload }, { call, put }) {
            const response = yield call(getForecastServer, payload);
            return response;
        },
        *realTimePredictor ({ payload }, { call, put }) {
            const response = yield call(realTimePredictor, payload);
            return response;
        },
        
        *save ({ payload }, { call, put }) {
            const response = yield call(save, payload);
            return response;
        },
        
        *history ({ payload }, { call, put }) {
            const response = yield call(history, payload);
            return response;
        },
        
        *validityHistogram ({ payload }, { call, put }) {
            const response = yield call(validityHistogram, payload);
            return response;
        },
        
        *predictor ({ payload }, { call, put }) {
            const response = yield call(predictor, payload);
            return response;
        },
        *batchPredictor ({ payload }, { call, put }) {
            const response = yield call(batchPredictor, payload);
            return response;
        },
        
        *validityQuery ({ payload }, { call, put }) {
            const response = yield call(validityQuery, payload);
            return response;
        },
        *points ({ payload }, { call, put }) {
            const response = yield call(points, payload);
            return response;
        },
        *psiReport ({ payload }, { call, put }) {
            const response = yield call(psiReport, payload);
            return response;
        },
        *lineChart ({ payload }, { call, put }) {
            const response = yield call(lineChart, payload);
            return response;
        },
        *effectiveness ({ payload }, { call, put }) {
            const response = yield call(effectiveness, payload);
            return response;
        },
        *stability ({ payload }, { call, put }) {
            const response = yield call(stability, payload);
            return response;
        },
        *modelDetails ({ payload }, { call, put }) {
            const response = yield call(modelDetails, payload);
            return response;
        },
        
        
    },
    
    reducers: {
        responseData (state, action) {
            return {
                ...state,
                ...action.payload,
            };
        }
    },
};
