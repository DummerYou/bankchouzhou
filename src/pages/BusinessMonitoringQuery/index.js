import React, { PureComponent, Fragment } from 'react';
import styles from './index.css';
import searchCss from '../../assets/search.css';
import { Input, Button, Icon, DatePicker, Table } from 'antd';
import { connect } from 'dva';
import moment from 'moment';
const { MonthPicker, RangePicker, WeekPicker } = DatePicker;

@connect(({ other }) => ({
  other,
}))
class BusinessMonitoringQuery extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      id: this.props.id ? this.props.id : null,
      name: '',
      description: '',
      createdTime: [],
      current: 1,
      size: 10,
      total: 0,
    };
  }

  componentDidMount() {
    const { id } = this.state;
  }
  onChange(date, dateString) {
    console.log(date, dateString);
  }
  search = () => {
    console.log('search');
    const { dispatch } = this.props;
    const { name, subappId, createdTime, current, size } = this.state;
    console.log(current);
    const search = {
      metricsId: [12, 34, 56],
      bucketId: "testBucket",
      materialId: "testMaterial",
      startTime: createdTime[0] ? moment(createdTime[0]).format('YYYY-MM-DD h:mm:ss') : '',
      endTime: createdTime[1] ? moment(createdTime[1]).format('YYYY-MM-DD h:mm:ss') : '',
      current,
      size,
    };
    console.log(search);
    dispatch({
      type: 'other/businessQuery',
      payload: search,
    }).then(res => {
      if (res.state) {
        console.log(res);
        if (res.resultInfo.data) {
          var data = res.resultInfo.data;
          this.setState({
            current: data.current,
            size: data.size,
            total: data.total,
            // resourceColumns: data.resourceColumns,
            // results: data.results,
          });
        }
      } else {
      }
    });
  };

  onChangeTime = (date, dateString) => {
    console.log(date, dateString);
    this.setState({
      createdTime: date,
    });
  };

  componentWillUnmount() {}
  reset = () => {
    this.setState({
      name: '',
      description: '',
      createdTime: [],
      current: '',
      size: '',
    });
  };

  render() {
    let list = [];
    for (let i = 0; i < 10; i++) {
      list.push({
        BucketId: 'BucketId',
        TC: 'TC',
        Traffic: 'Traffic',
        Sales: 'Sales',
        TA: 'TA',
        IncTA: 'IncTA',
        SPT: 'SPT',
        IncSPT: 'IncSPT',
        mzl: 1,
        zhl: 2,
        createTiStr: '2021',
      });
    }
    const columns = [
      {
        title: 'BucketId',
        dataIndex: 'BucketId',
        align: 'center',
      },
      {
        title: 'TC',
        dataIndex: 'TC',
        align: 'center',
      },
      {
        title: 'Traffic',
        dataIndex: 'Traffic',
        align: 'center',
      },
      {
        title: 'Sales(K)',
        dataIndex: 'Sales',
        align: 'center',
      },
      {
        title: 'TA',
        dataIndex: 'TA',
        align: 'center',
      },
      {
        title: 'IncTA',
        dataIndex: 'IncTA',
        align: 'center',
      },
      {
        title: 'SPT',
        dataIndex: 'SPT',
        align: 'center',
      },
      {
        title: 'IncSPT',
        dataIndex: 'IncSPT',
        align: 'center',
      },
      {
        title: '命中率（%）',
        dataIndex: 'mzl',
        align: 'center',
      },
      {
        title: '转化率（%）',
        dataIndex: 'zhl',
        align: 'center',
      },
      {
        title: '时间',
        dataIndex: 'createTiStr',
        align: 'center',
        width: 200,
      },
    ];

    const pagination = {
      current: 1,
      pageSize: 20,
      total: 99,
      showQuickJumper: true,
      onChange: (date, dateString) => {
        console.log(date);
      },
    };
    return (
      <div className={styles.bmq}>
        <div className={searchCss.search}>
          <div className={searchCss.searchs}>
            <div className={searchCss.searchItem}>
              <span>业务指标：</span>
              <Input
                className={searchCss.serachIpt}
                placeholder="请输入业务指标"
                value={this.state.name}
                onChange={e => this.setState({ name: e.target.value })}
                suffix={<Icon type="search" />}
              />
            </div>
          </div>
          <div className={searchCss.searchs}>
            <div className={searchCss.searchItem}>
              <span>时间选择：</span>
              <RangePicker value={this.state.createdTime} onChange={this.onChangeTime} />
            </div>
          </div>
          <div className={searchCss.searchs}>
            <div className={searchCss.searchItem}>
              <Button className={searchCss.r20} onClick={this.reset}>
                重置
              </Button>
              <Button type="primary" onClick={this.search}>
                搜索
              </Button>
            </div>
          </div>
        </div>
        <div className={styles.table}>
          <Table columns={columns} dataSource={list} pagination={pagination} />
        </div>
      </div>
    );
  }
}

export default BusinessMonitoringQuery;
