let dataJson = {
	"code": 100000,   
	"msg": "success",
	"data": {
		"current": 3,  //当前页码数
		"size": 10,	 //每页条数
		"total": 59,	 //总数
		"resourceColumns": [	//资源名列表
			"hadoop",
			"k8s",
			"flink"
		],
		"results": [{
				"appId": "app1",//应用名（场景名）
				"subappId": "subapp1",//子应用名(子场景名)
				"createTime": "2021-05-01 18:30:00", //创建时间
				"resources": [{
						"name": "hadoop",//hadoop资源
						"value": [{
								"metricsName": "cpu",	//cpu
								"value": 23				//核数
							},
							{
								"metricsName": "mem",  //内存
								"value": 45			  //内存大小
							}
						]
					},
					{
						"name": "k8s",		//k8s资源
						"value": [{
								"metricsName": "gpu",	//gpu
								"value": 23				//核数
							},
							{
								"metricsName": "mem",	//内存
								"value": 45				//内存大小
							}
						]
					}
				]
			},
			{
				"appId": "app1",//应用名（场景名）
				"subappId": "subapp2",//子应用名(子场景名)
				"creatTime": "2021-05-01 18:30:00",//创建时间
				"resources": [{
					"name": "k8s",//hadoop资源
					"value": [{
							"metricsName": "cpu",//cpu
							"value": 23			//核数
						},
						{
							"metricsName": "mem",//内存
							"value": 45			//内存大小
						}
					]
				}]
			}
		]
	}
}

export default dataJson;