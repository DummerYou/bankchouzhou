import React, { PureComponent, Fragment } from 'react';
import styles from './index.css';
import { Input, Button, Icon, Table,Tabs } from 'antd';
import { connect } from 'dva';
import AddItem1 from "../../components/addItem/addItem1"
import AddItem2 from "../../components/addItem/addItem2"
import AddItem3 from "../../components/addItem/addItem3"
import AddItem4 from "../../components/addItem/addItem4"

const { TabPane } = Tabs;

@connect(({ other }) => ({
  other,
}))
class AddItem extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      defaultActiveKey:"1"
    };
  }

  componentDidMount() {
    if(document.getElementById('headerName')){
      document.getElementById('headerName').innerHTML = '>新建模型监控';
    }
  }
  changeTabPosition = tabPosition => {
    this.setState({ tabPosition });
  };
  callback = (key) => {
    console.log(key);
    this.setState({
      defaultActiveKey: key,
    });
  }
  componentWillUnmount() {}
  setActiveKey = () => {
    const { defaultActiveKey} = this.state;
    console.log('object')
    this.setState({
      defaultActiveKey: defaultActiveKey*1+1+"",
    });
    this.forceUpdate();
  }

  render() {
    const { defaultActiveKey} = this.state;
    return (
      <div className={'main'}>
      <Tabs className={styles.Tabs + ' addTabs'} activeKey={defaultActiveKey} tabPosition="left"  type="card" onChange={this.callback}>
      <TabPane tab="1 基本信息" key="1">
        <AddItem1 setActiveKey={this.setActiveKey} />
      </TabPane>
      <TabPane tab="2 选择模型预估应用" key="2">
        <AddItem2 setActiveKey={this.setActiveKey}/>
      </TabPane>
      <TabPane tab="3 模型监控" key="3">
        <AddItem3 />
      </TabPane>
      <TabPane tab="4 资源配置" key="4">
        <AddItem4 />
      </TabPane>
    </Tabs>
      </div>
    );
  }
}
export default AddItem;
