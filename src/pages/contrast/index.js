import React, { useState, useEffect } from 'react';
import { Tabs } from 'antd';
import SecondCategory from '../../components/secondCategory/index'
import MoreClassify from '../../components/moreClassify/index'
import Flyback from '../../components/flyback/index'
import styles from './index.css';
const { TabPane } = Tabs;

const Contrast = () => {
  const callback =(key)=>{
    console.log(key);
  }
  return (
    <>
      <Tabs defaultActiveKey="1" onChange={callback} tabPosition='left'>
        <TabPane tab="模型迭代对比-二分类" key="1">
          <SecondCategory/>
        </TabPane>
        <TabPane tab="模型迭代对比-多分类" key="2">
          <MoreClassify/>
        </TabPane>
        <TabPane tab="模型迭代对比-回归" key="3">
          <Flyback/>
        </TabPane>
      </Tabs>
    </>
  );
};
export default Contrast;
