import React, { PureComponent, Fragment } from 'react';
import styles from './index.css';
import { Input, Button, Icon, Table,Tabs } from 'antd';
import { connect } from 'dva';
import Detail1 from "../../components/Detail/Detail1"
import Detail2 from "../../components/Detail/Detail2"
import Detail3 from "../../components/Detail/Detail3"
import Detail4 from "../../components/Detail/Detail4"
import Detail5 from "../../components/Detail/Detail5"
import Detail6 from "../../components/Detail/Detail6"

const { TabPane } = Tabs;

@connect(({ other }) => ({
  other,
}))
class detail extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      defaultActiveKey:"1"
    };
  }

  componentDidMount() {
    console.log(this.props.location.query.name);
    let name = ""
    try {
      name = " _ " + this.props.location.query.name
    } catch (error) {
      
    }
    if(document.getElementById('headerName')){
      document.getElementById('headerName').innerHTML = ' > 模型监控' + name;
    }
  }
  changeTabPosition = tabPosition => {
    this.setState({ tabPosition });
  };
  callback = (key) => {
    console.log(key);
  }
  componentWillUnmount() {}
 

  render() {
    const { defaultActiveKey} = this.state;
    return (
      <div className={'main'}>
      <Tabs className={styles.Tabs} defaultActiveKey={defaultActiveKey}  onChange={this.callback}>
      <TabPane tab="模型监控报告" key="1">
        <Detail1 />
        <Detail3 />
        <Detail4 />
        <Detail5 />
      </TabPane>
      <TabPane tab="运行历史" key="2">
        <Detail2 />
      </TabPane>
      <TabPane tab="配置" key="3">
        <Detail6 />
      </TabPane>
    </Tabs>
      </div>
    );
  }
}
export default detail;
