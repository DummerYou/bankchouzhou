let dataJson = {
    "code": 200,
  
    "message": "请求成功",
  
    "data": {
        featureId: 123123, //特征ID
        name: '用户画像', //特征名称
        type: 'feature', //特征类型（特征 or 物料）
        description: '用户画像特征', //特征描述
        path: 'KFC/特征中心/Feeds流场景', //特征路径
        relation: [123, 456, 78], //特征关系（预留）
        citation: ['app1', 'app2'], //特征引用情况（预留）
        createdTime: '2021-05-14 12:00:34', //创建时间
        createdUser: '4pdadmin', //创建用户
        tables: [
            {
                id: 1231231,
                name: 'test.table1', //数据表名称
                description: 'testtestesttesttest', //数据表描述
                storageType: 'HDFS', //数据存储类型
                storageUrl: 'hdfs://10.0.74.121:8025/user/hive/warehouse/jv_temp.db/a.csv', //数据存储地址
                storageParam: '{"username":"retail","password":"123456"}', //数据存储参数
                schema: [
                  //数据schema
                  {
                    name: 'REQ_ID', //字段名称
                    type: 'String', //字段类型
                    detail: '123123', //字段具体含义
                  },
                  {
                    name: 'REQ_ID', //字段名称
                    type: 'String', //字段类型
                    detail: '123123', //字段具体含义
                  },
                  {
                    name: 'REQ_ID', //字段名称
                    type: 'String', //字段类型
                    detail: '123123', //字段具体含义
                  },
                  {
                    name: 'REQ_ID', //字段名称
                    type: 'String', //字段类型
                    detail: '123123', //字段具体含义
                  },
                  {
                    name: 'REQ_ID', //字段名称
                    type: 'String', //字段类型
                    detail: '123123', //字段具体含义
                  },
                  {
                    name: 'REQ_ID', //字段名称
                    type: 'String', //字段类型
                    detail: '123123', //字段具体含义
                  },
                ],
                createdUser: '4pdadmin', //创建人
                createdTime: '2021-05-11 02:20:34', //创建时间
                updatedUser: '4pdadmin', //更新人
                updatedTime: '202-05-12 15:40:34', //更新时间
              },{
                id: 123123132,
                name: 'test.table2', //数据表名称
                description: 'testtestesttesttest', //数据表描述
                storageType: 'HDFS', //数据存储类型
                storageUrl: 'hdfs://10.0.74.121:8025/user/hive/warehouse/jv_temp.db/a.csv', //数据存储地址
                storageParam: '{"username":"retail","password":"123456"}', //数据存储参数
                schema: [
                  //数据schema
                  {
                    name: 'REQ_ID', //字段名称
                    type: 'String', //字段类型
                    detail: '123123', //字段具体含义
                  },
                  {
                    name: 'REQ_ID', //字段名称
                    type: 'String', //字段类型
                    detail: '123123', //字段具体含义
                  },
                  {
                    name: 'REQ_ID', //字段名称
                    type: 'String', //字段类型
                    detail: '123123', //字段具体含义
                  },
                  {
                    name: 'REQ_ID', //字段名称
                    type: 'String', //字段类型
                    detail: '123123', //字段具体含义
                  },
                ],
                createdUser: '4pdadmin', //创建人
                createdTime: '2021-05-11 02:20:34', //创建时间
                updatedUser: '4pdadmin', //更新人
                updatedTime: '202-05-12 15:40:34', //更新时间
              },{
                id: 1232341233,
                name: 'test.table3', //数据表名称
                description: 'testtestesttesttest', //数据表描述
                storageType: 'HDFS', //数据存储类型
                storageUrl: 'hdfs://10.0.74.121:8025/user/hive/warehouse/jv_temp.db/a.csv', //数据存储地址
                storageParam: '{"username":"retail","password":"123456"}', //数据存储参数
                schema: [
                  //数据schema
                  {
                    name: 'REQ_ID', //字段名称
                    type: 'String', //字段类型
                    detail: '123123', //字段具体含义
                  },
                  {
                    name: 'REQ_ID', //字段名称
                    type: 'String', //字段类型
                    detail: '123123', //字段具体含义
                  },
                  {
                    name: 'REQ_ID', //字段名称
                    type: 'String', //字段类型
                    detail: '123123', //字段具体含义
                  },
                ],
                createdUser: '4pdadmin', //创建人
                createdTime: '2021-05-11 02:20:34', //创建时间
                updatedUser: '4pdadmin', //更新人
                updatedTime: '202-05-12 15:40:34', //更新时间
              },{
                id: 1231546234,
                name: 'test.table4', //数据表名称
                description: 'testtestesttesttest', //数据表描述
                storageType: 'HDFS', //数据存储类型
                storageUrl: 'hdfs://10.0.74.121:8025/user/hive/warehouse/jv_temp.db/a.csv', //数据存储地址
                storageParam: '{"username":"retail","password":"123456"}', //数据存储参数
                schema: [
                  //数据schema
                  {
                    name: 'REQ_ID', //字段名称
                    type: 'String', //字段类型
                    detail: '123123', //字段具体含义
                  },
                ],
                createdUser: '4pdadmin', //创建人
                createdTime: '2021-05-11 02:20:34', //创建时间
                updatedUser: '4pdadmin', //更新人
                updatedTime: '202-05-12 15:40:34', //更新时间
              },
        ],
      }
  }
  export default dataJson