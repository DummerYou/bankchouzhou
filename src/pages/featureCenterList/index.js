import styles from './index.css';
import React, { PureComponent, Fragment } from 'react';
import {
  Icon,
  Input,
  Button,
  Form,
  Checkbox,
  message,
  Tabs,
  Breadcrumb,
  Row,
  Col,
  Typography,
  Table,
} from 'antd';
import Link from 'umi/link';
import router from 'umi/router';
import { connect } from 'dva';
import dataJson from './aa.js';

import { mobileValidator, setStorage, getStorage, rmStorage, decrypt } from '../../services/utils';
const FormItem = Form.Item;
const { TabPane } = Tabs;
const { Paragraph } = Typography;

@connect(({ feature }) => ({
  feature,
}))
@Form.create()
class FeatureCenterList extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      showType: '1',
      rememberMe: true,
      downSeconds: 0,
      listState: '',
      dataItem: {
        tables: [],
      },
    };
  }

  componentDidMount() {
    const {
      dispatch,
      location: { query },
    } = this.props;
    const data = { id: query.id };
    console.log(query);
    
    // this.setState({
    //   dataItem: dataJson.data,
    // });
    dispatch({
      type: 'feature/getFusionFeature',
      payload: data,
    }).then(res => {
      console.log(res);
      if (res.code == 200) {
        console.log(res.data);
        this.setState({
          dataItem: res.data,
        });
      } else {
      }
    });
  }

  componentWillUnmount() {}

  onChange = val => {
    this.setState({ rememberMe: !this.state.rememberMe });
  };
  setListState = val => {
    this.setState({ listState: val });
    this.forceUpdate();
  };

  render() {
    const {
      form: { getFieldDecorator, getFieldValue },
      submitting,
    } = this.props;
    const { dataItem } = this.state;
    const columns = [
      {
        title: '序号',
        dataIndex: 'index',
        key: 'index',
        render(text, record, index) {
          return <span>{index + 1}</span>;
        },
      },
      {
        title: '字段名',
        dataIndex: 'name',
        key: 'name',
      },
      {
        title: '字段类型',
        dataIndex: 'type',
        key: 'type',
      },
      {
        title: '字段业务定义',
        key: 'description',
        dataIndex: 'description',
      },
    ];
    const data = dataItem;
    // console.log(submitting)
    if (!data.name) {
      return (
        <div className={styles.main}>
        <Breadcrumb separator=">">
          <Breadcrumb.Item>
            <a href="/home" className={styles.bread1}>特征列表</a>
          </Breadcrumb.Item>
          <Breadcrumb.Item className={styles.bread2}>详情</Breadcrumb.Item>
        </Breadcrumb>
        </div>
      );
    }
    return (
      <div className={styles.main}>
        <Breadcrumb separator=">">
          <Breadcrumb.Item>
            <a href="/home" className={styles.bread1}>特征列表</a>
          </Breadcrumb.Item>
          <Breadcrumb.Item className={styles.bread2}>详情</Breadcrumb.Item>
        </Breadcrumb>
        <div className={styles.infoList}>
          用户画像
        </div>
        <div className={styles.fileInfo}>
          <div className={styles.fileInfos1}>
            <div className={styles.fileInfosDiv}>文件路径<span>{data.path}</span></div>
            <div className={styles.fileInfosDiv}>包含数据<span>{data.tables.length}个数据表</span></div>
            <div className={styles.fileInfosDiv}>特征创建时间<span>{data.createdTime}</span></div>
          </div>
          <div className={styles.fileInfos2}>
          <div className={styles.fileInfosDiv}>创建人<span>{data.createdBy}</span></div>
          <div className={styles.fileInfosDiv}>文件描述<span>{data.description}</span></div>
          </div>
        </div>
        <div className={styles.pointAnchor}>
          <Tabs  className={styles.pointAnchors}
          onChange={(e) => {
            this.setListState(e);
          }}>
            <TabPane  tab="全部" key=""
              href="#"
              className={this.state.listState === '' ? styles.pointAnchorsA : ''}
            >
            </TabPane>
            {(data.tables && data.tables.length)
              ? data.tables.map((item, index) => {
                  return (
                    <TabPane tab={item.name} key={item.id}
                      // href={'#' + item.id}
                      // key={index}
                      // className={this.state.listState === item.id ? styles.pointAnchorsA : ''}
                      // onClick={() => {
                      //   this.setListState(item.id);
                      // }}
                    >
                      
                    </TabPane>
                  );
                }, this)
              : null}
          </Tabs>
        </div>
        {(data.tables && data.tables.length)
          ? data.tables.map((item, index) => {
            console.log(this.state.listState,item.id)
              if (this.state.listState === '' || this.state.listState == item.id) {
                return (
                  <div className={styles.tables} key={item.id}>
                    <div className={styles.tablesTop}>
                    <div className={styles.tablesttt}>
                    <div className={styles.tablesTitle}>{item.name}</div>
                      <div className={styles.textRight}>
                          {this.state.listState != item.id ? (
                            <Button
                              type="link"
                              className={styles.colBtn}
                              onClick={() => {
                                this.setListState(item.id);
                              }}
                            >
                              查看详情
                            </Button>
                          ) : (
                            ''
                          )}
                          <Button type="link" className={styles.colBtn}>
                            数据查询
                          </Button>
                        </div>
                    </div>
                      <Row gutter={16}>
                        <Col span={8}>
                          <Paragraph ellipsis>数据表名：{item.name}</Paragraph>
                        </Col>
                        <Col span={8}>
                          <Paragraph ellipsis>存储引擎：{item.storageType}</Paragraph>
                        </Col>
                        <Col span={8}>
                          <Paragraph ellipsis>创建时间：{item.createdTime}</Paragraph>
                        </Col>
                        <Col span={8}>
                          <Paragraph ellipsis>创建人：{item.createdBy}</Paragraph>
                        </Col>
                        <Col span={8}>
                          <Paragraph ellipsis>数据大小：{item.recordNum}</Paragraph>
                        </Col>
                        <Col span={8}>
                          <Paragraph ellipsis>更新时间：{item.updatedTime}</Paragraph>
                        </Col>
                        <Col span={8}>
                          <Paragraph ellipsis>
                            数据条数：{(item.schemaData && item.schemaData.length) || 0}
                          </Paragraph>
                        </Col>
                        <Col span={8}>
                          <Paragraph ellipsis>数据地址：{item.storageUrl}</Paragraph>
                        </Col>
                        <Col span={8}>
                          <Paragraph ellipsis>数据描述：{item.description}</Paragraph>
                        </Col>
                        
                      </Row>
                    </div>
                    <Table
                      columns={columns}
                      indentSize={0}
                      dataSource={item.schemaData}
                      pagination={false}
                      rowKey="name"
                      size="middle"
                    />
                  </div>
                );
              } else {
                return null;
              }
            }, this)
          : null}
      </div>
    );
  }
}

export default FeatureCenterList;
