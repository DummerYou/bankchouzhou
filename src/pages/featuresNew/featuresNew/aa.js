let dataJson = {
  code: 200,

  message: '请求成功',

  data: {
    name: null,

    createTime: null,

    updateTime: null,

    lineNum: 382,

    dataSize: 123232,

    //表结构

    fieldSchema: [
      {
        name: 'job',

        type: 'String',

        description: null,
      },

      {
        name: 'age',

        type: 'String',

        description: null,
      },

      {
        name: 'month',

        type: 'String',

        description: null,
      },
    ],
  },
};
export default dataJson;
