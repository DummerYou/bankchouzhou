import styles from './index.css';
import React, { PureComponent, Fragment } from 'react';
import {
  Icon,
  Input,
  Button,
  Form,
  Checkbox,
  message,
  Tabs,
  Breadcrumb,
  Row,
  Col,
  Typography,
  Table,
} from 'antd';
import Link from 'umi/link';
import router from 'umi/router';
import { connect } from 'dva';
import dataJson from './aa.js';

import { mobileValidator, setStorage, getStorage, rmStorage, decrypt } from '../../services/utils';
const FormItem = Form.Item;
const { TabPane } = Tabs;
const { Paragraph } = Typography;

@connect(({ feature }) => ({
  feature,
}))
@Form.create()
class featuresNew extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      schema: {
        fieldSchema: [],
      },
    };
  }

  componentDidMount() {
    const { dispatch } = this.props;
    var connectionConfig = sessionStorage.getItem('connectionConfig');
    if (connectionConfig) {
      const data = {
        connectionType: '88',
        connectionConfig: connectionConfig,
      };
      
      this.setState({
        schema: dataJson.data,
        schemaList: dataJson.data.fieldSchema,
      });
      dispatch({
        type: 'feature/getDataSchema',
        payload: data,
      }).then(res => {
        console.log(res);
        if (res.code === 200) {
          this.setState({
            schema: res.data,
            schemaList: res.data.fieldSchema,
          });
        } else {
        }
      });
    } else {
    }
  }

  componentWillUnmount() {}

  hasOk = val => {
    const { dispatch } = this.props;
    const { schema, schemaList } = this.state;
    const connectionConfigItem = JSON.parse(sessionStorage.getItem('connectionConfigItem'));
    console.log(connectionConfigItem);
    const data = {
      connectionConfig: sessionStorage.getItem('connectionConfig'),
      connectionType: '88',
      featureId: connectionConfigItem.featureId,
      name: schema.name,
      schemaData: schemaList,
      storageEngine: connectionConfigItem.storageEngine,
    };
    dispatch({
      type: 'feature/registerFusionFeature',
      payload: data,
    }).then(res => {
      console.log(res);
      if (res.code === 200) {
        router.push('/home');
        message.success('新增特征成功');
      } else {
        message.error(res.message);
      }
    });
  };
  reset = val => {
    console.log(val);
    router.push('/home');
  };

  render() {
    const {
      form: { getFieldDecorator, getFieldValue },
      submitting,
    } = this.props;
    const { schema, schemaList } = this.state;
    const columns = [
      {
        title: '序号',
        dataIndex: 'index',
        key: 'index',
        render(text, record, index) {
          return <span>{index + 1}</span>;
        },
      },
      {
        title: '字段名',
        dataIndex: 'name',
        key: 'name',
      },
      {
        title: '字段类型',
        dataIndex: 'type',
        key: 'type',
      },
      {
        title: '字段业务定义',
        key: 'description',
        dataIndex: 'description',
        render: (text, recode, key) => (
          <Input
            onChange={e => {
              e.persist();
              schemaList[key].description = e.target.value;
              this.setState({
                schemaList: schemaList,
              });
            }}
          />
        ),
      },
    ];
    return (
      <div className={styles.main}>
        <Breadcrumb separator=">">
          <Breadcrumb.Item>
            <a href="/home"  className={styles.bread1}>特征列表</a>
          </Breadcrumb.Item>
          <Breadcrumb.Item  className={styles.bread2}>新增特征</Breadcrumb.Item>
        </Breadcrumb>
        <div className={styles.infoList}>新增特征</div>
        <div className={styles.tables}>
          <Table
            columns={columns}
            indentSize={0}
            dataSource={schemaList}
            pagination={false}
            rowKey="name"
            size="middle"
          />
        </div>
        <div className={styles.textRight}>
          <Button onClick={this.reset} className={styles.r20}>
            上一步
          </Button>
          <Button type="primary" onClick={this.hasOk}>
            确定
          </Button>
        </div>
      </div>
    );
  }
}

export default featuresNew;
