import React, { PureComponent, Fragment } from 'react';
import styles from './index.css';
import {Button, Table ,Modal,message,Card} from 'antd';
import router from 'umi/router';
import { setCook } from '../services/utils';
import { connect } from 'dva';
import moment from 'moment';
import dataJson from './a.js';
import axios from 'axios'
const { confirm } = Modal;

@connect(({ other }) => ({
  other,
}))
class Index extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      results:[],
      current: 1,
      size: 10,
      total: 0,
      isModalVisible:false
    };
  }

  componentDidMount() {
    this.search();
    
  }


  componentWillUnmount() {}
  addTo = () => {
    // router.push('/addItem');
    // router.push('/newCreate');
    this.setState({
      isModalVisible:true,
    })
  }
  handleCancel = () => {
    this.setState({
      isModalVisible:false,
    })
  }
  //详情
  handClick= record => {
    // router.push('/detail?id='+record.id)
    const { dispatch} = this.props;
    const {id,serverType,serverName}  = record
    if(serverType == 1){
      dispatch({
        type: 'feature/predictor',
        payload: {
          serverId: id,
        },
      }).then(res => {
        console.log(res);
        if (res.code == 200) {
          router.push('/detail?id='+record.id + "&name=" + serverName)
        }else{
          if(res.data){
            message.error(res.data.message)
          }else{
            message.error("接口调用失败")
          }
        }
      });
    }else{
    axios.get('http://10.104.1.142:8081/model-iteration/v1/getResultDetail',{
      params:{
        serveId:id
      }
    }).then((res)=>{
      if(res.data.code != 200){
        message.error(res.data.message)
        return
      }
       const {data} = res.data
       const {type,content} = data;
       console.log('type',type)
       console.log('content',content)
       if(type===3){
         router.push({
           pathname:'./secondCategory',
           query: {name:serverName},
           state:content
         })
       }else if(type===4){
        router.push({
          pathname:'./moreClassify',
          query: {name:serverName},
          state:content
        })
       }else{
        router.push({
          pathname:'./flyBack',
          query: {name:serverName},
          state:content
        })
       }
    }).catch((err)=>{
      console.log(err);
      // message.error('请求错误')
    })
  }
  }

  search = num => {
    const { current, size } = this.state;
    const { dispatch} = this.props;
    dispatch({
      type: 'feature/query',
      payload: {
        current: num ? num : current,
        size,
      },
    }).then(res => {
      console.log(res);
      if (res.code==200) {
          var data = res.data;
          this.setState({
            current: data.current,
            size: data.size,
            total: data.total,
            results: data.records,
          });
      }
    })
  }

  deleteClick(id) {
    const { dispatch } = this.props;
    const _this = this;
    confirm({
      title: '您确定要删除该数据吗?',
      okText: '确定',
      okType: 'danger',
      cancelText: '取消',
      onOk() {
        console.log('OK');

        dispatch({
          type: 'feature/serverDelete',
          payload: { id: id },
        }).then(res => {
          console.log(res);
          if (res.code == 200) {
            console.log(res);
            message.success('删除成功');
            _this.search();
          } else {
            message.error(res.message);
          }
        });
      },
      onCancel() {
        console.log('Cancel');
      },
    });
  }
  render() {
    const { current, size, total,results,isModalVisible} = this.state;
    console.log(results)
    let list = [
      {
        key: '1',
        name: 'John Brown',
        age: 32,
        address: 'New York No. 1 Lake Park',
        address1: 11,
        address2: 21,
        tags: ['nice', 'developer'],
      },
      {
        key: '2',
        name: 'Jim Green',
        age: 42,
        address: 'London No. 1 Lake Park',
        address1: 12,
        address2: 22,
        tags: ['loser'],
      },
      {
        key: '3',
        name: 'Joe Black',
        age: 321,
        address: 'Sidney No. 1 Lake Park',
        address1: 13,
        address2: 23,
        tags: ['cool', 'teacher'],
      }]

    var columns = [
      {
        title: '应用名称',
        dataIndex: 'serverName',
        key: 'serverName',
      },
      {
        title: '应用类型',
        dataIndex: 'serverType',
        key: 'serverType',
        render: (text, record) => (
          text==1?<span>模型监控</span>:<span>模型迭代</span>
        ),
      },
      {
        title: '版本数量',
        dataIndex: 'address',
        key: 'address',
      },
      {
        title: '创建时间',
        key: 'createdTime',
        dataIndex: 'createdTime',
      },
      {
        title: '创建人',
        key: 'createdBy',
        dataIndex: 'createdBy',
      },
      {
        title: '操作',
        key: 'action',
        render: (text, record) => (
          <span className={'tableAction'}>
            <Button type="link" onClick={()=>this.handClick(record)}>查看</Button>
            <Button type="link" onClick={() => this.deleteClick(record.id)}>删除</Button>
          </span>
        ),
      },
    ];


    const pagination = {
      current: current,
      pageSize: size,
      total: total,
      showQuickJumper: true,
      onChange: (date, dateString) => {
        console.log(date);
        this.setState(
          {
            current: date,
          },
          this.search(date),
        );
      },
    };
    return (
      <div className={'main'}>
        <div className={styles.header}>
          <div className={styles.headerl}>应用管理</div>
          <div className={styles.headerr}>
            <Button type="primary" onClick={this.addTo}>添加</Button>
          </div>
        </div>
        <Table className={styles.table + ' table1'} columns={columns} dataSource={results} pagination={pagination} size="middle" />
        <Modal
          title="新建应用"
          visible={isModalVisible}
          onCancel={this.handleCancel}
          className={'addModalIndex'}
        >
            <Card style={{ width: 180 }} onClick={()=>{router.push('/addItem')}}>
              模型监控
            </Card>
            <Card style={{ width: 180 }} onClick={()=>{router.push('/newCreate')}}>
              模型迭代对比
            </Card>
        </Modal>
      </div>
    );
  }
}
export default Index;
