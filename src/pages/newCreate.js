import React, { useState, useEffect } from 'react';
import { Form, Input, Button, Checkbox, Select, Modal, message } from 'antd';
import styles from './index.css';
import { PlusOutlined, MinusOutlined } from '@ant-design/icons';
import router from 'umi/router';
import axios from 'axios';

const { Option } = Select;

const NewCreate = props => {
  const { form } = props;
  const { getFieldDecorator } = form;
  const [isModalVisible, setIsModalVisible] = useState(true);
  const [modelList, setModelList] = useState([]);
  const [tableList, setTableList] = useState([]);
  const [dynamicFormItem, setDynamicFormItem] = useState([]);
  const [formItem, setFormItem] = useState([]);
  const [param, setParam] = useState({
    page: 1,
    size: 100,
  });
  let marketId = 0;
  useEffect(() => {
    (async () => {
      // await
      axios
        .get('http://10.104.1.142:8081/model-iteration/v1/getModel', {
          params: {
            ...param,
          },
        })
        .then(res => {
          const { data: list } = res.data;
          const { data } = list || {};
          setModelList(data);
        })
        .catch(err => {
          message.error('请求错误');
        });
    })();
  }, [param]);
  useEffect(() => {
    (async () => {
      await axios
        .get('http://10.104.1.142:8081/model-iteration/v1/getTable', {
          params: {
            ...param,
          },
        })
        .then(res => {
          const { data: list } = res.data;
          const { data } = list || {};
          setTableList(data);
        })
        .catch(err => {
          message.error('请求错误');
        });
    })();
  }, [param]);

  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 5 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 19 },
    },
  };
  //mode的确认按钮
  const handleOk = async(e) => {
    const tableItem =tableList.find((item)=>{
      return item.title===props.form.getFieldValue('table')
    })
    console.log(formItem);
    let formItems = formItem.filter(v=>v)
    props.form.setFieldsValue({model:formItems})
    // props.form.setFieldsValue({table:tableItem})
    props.form.validateFields();
    await axios
    .post('http://10.104.1.142:8081/model-iteration/v1/save', {
        serverName:props.form.getFieldValue('username'),
        modelList:formItems,
        // table:props.form.getFieldValue('table')
        table:tableItem
      }
    )
    .then(res => {
      // console.log(res.data.code);
      if(res.data.code == 200){
        router.push('/');
        message.success(res.data.message);
      }else{
        
      message.error(res.data.message);
      }
    })
    .catch(err => {
      message.error('新建失败');
    });
    // router.push('/');
  };
  const handleCancel = () => {
    router.go(-1);
  };
    //添加
  const add = e => {
    e.preventDefault();
    const newDynamicForm = dynamicFormItem.concat([Date.now()]);
    setDynamicFormItem(newDynamicForm);
  };
  //删除
  const detlll = (e,index) => {
    console.log(e,index);
    e.preventDefault();
    console.log(dynamicFormItem);
    const newDynamicForm = dynamicFormItem.concat([null])
    newDynamicForm[index] = null
    setDynamicFormItem(newDynamicForm);
    console.log(dynamicFormItem);
    setFormItem(pre => {
      pre[index+1] = null
      console.log(pre)
      return pre
    });
  };
  
    //数组去重
  const modeltype = (values,index) => {
    console.log(values,formItem,index);
    // const v = formItem.find(item => item === values);
    // console.log(v);
    // if (v === undefined) {
    //   console.log(v);
    //   const newData = modelList.find(item => {
    //     return item.title === values;
    //   });
    //   setFormItem(pre => [...pre, newData]);
    // }
      const newData = modelList.find(item => {
        return item.title === values;
      });
      setFormItem(pre => {
        pre[index] = newData
        console.log(pre)
        return pre
      });
  };
  console.log('formItem',formItem)

  const addForm = (index) => {
    return (
      <>
        <Select
          style={{ width: '350px' }}
          placeholder="请选择需要对比的模型"
          allowClear
          onChange={values => {
            modeltype(values,index)
          }}
        >
          {modelList.map((item, index) => {
            return (
              <Option key={index + 1} value={item.title}>
                {item.title}
              </Option>
            );
          })}
        </Select>
      </>
    );
  };

  return (
    <Modal
      title="模型迭代对比配置"
      visible={isModalVisible}
      onOk={handleOk}
      onCancel={handleCancel}
    >
      <Form onSubmit={handleOk} {...formItemLayout}>
        <Form.Item label="应用名称">
          {getFieldDecorator('username', {
            rules: [{ required: true, message: '应用名称必填字段' }],
          })(<Input placeholder="请输入应用名称" />)}
        </Form.Item>
        <Form.Item label="选择模型">
          {getFieldDecorator('model', {
            rules: [{ required: true, message: '选择模型必选必填字段' }],
          })(
            <div>
              <div className={styles.modelSelect}>
                {addForm(0)}
                <PlusOutlined style={{ padding: '10px' }} onClick={e => add(e)} />
              </div>
              <div>
                {dynamicFormItem.map((item, index) => {
                  // console.log(item);
                  if(item){
                    return (
                      <div key={item} style={{ display: 'flex', marginTop: '8px' }}>
                        {addForm(index+1)}
                        <MinusOutlined onClick={e => detlll(e,(index))} style={{ padding: '10px' }} />
                      </div>
                    );
                  }else{
                    return null
                  }
                })}
              </div>
            </div>,
          )}
        </Form.Item>
        <Form.Item label="选择测试数据">
          {getFieldDecorator('table', {
            rules: [{ required: true, message: '测试数据必选' }],
          })(
            <Select placeholder="请选择评价模型迭代效果的测试数据" allowClear>
              {tableList.map((item, index) => {
                return (
                  <Option key={index + 1} value={item.title}>
                    {item.title}
                  </Option>
                );
              })}
            </Select>,
          )}
        </Form.Item>
      </Form>
    </Modal>
  );
};
export default Form.create()(NewCreate);
