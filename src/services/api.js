
import request from './http';

// 1.服务列表
export async function login(params) {
    return request(`/model-server/v1/query`, {
        method: 'POST',
        body: params,
        postType: 1
    });
}
// 1.服务列表
export async function query(params) {
    return request(`/model-server/v1/query`, {
        method: 'POST',
        body: params,
        postType: 1
    });
}
// 1.服务列表删除
export async function serverDelete(params) {
    return request(`/model-server/v1/delete`, {
        method: 'DELETE',
        body: params,
        postType: 1
    });
}

// 2.获取自定义dag
export async function getDag(params) {
    return request(`/model-monitor/v1/getDag`, {
        method: 'GET',
        params: params,
        // postType: 1
    });
}
// 2.获取自定义dag
export async function getDag1(params) {
    return request(`/model-monitor/v1/Dag/stability`, {
        method: 'GET',
        params: params,
        // postType: 1
    });
}
// 2.获取自定义dag
export async function getDag2(params) {
    return request(`/model-monitor/v1/Dag/effectiveness`, {
        method: 'GET',
        params: params,
        // postType: 1
    });
}
// 获取模型预估应用
export async function getForecastServer(params) {
    return request(`/model-monitor/v1/getForecastServer`, {
        method: 'GET',
        params: params,
        // postType: 1
    });
}
// 1、实时预估

export async function realTimePredictor(params) {
    return request(`/model-monitor/v1/realTimePredictor`, {
        method: 'GET',
        params: params,
        // postType: 1
    });
}
//模型监控自定义dag保存
export async function save(params) {
    return request(`/model-monitor/v1/save`, {
        method: 'POST',
        body: params,
        postType: 1
    });
}
// 运行历史
export async function history(params) {
    return request(`/model-monitor/v1/history`, {
        method: 'GET',
        params: params,
        // postType: 1
    });
}
// 运行历史-有效性性
export async function effectiveness(params) {
    return request(`/model-monitor/v1/history/effectiveness`, {
        method: 'POST',
        body: params,
        postType: 1
    });
}
// 运行历史-稳定性
export async function stability(params) {
    return request(`/model-monitor/v1/history/stability`, {
        method: 'POST',
        body: params,
        postType: 1
    });
}
// 5、模型有效性监控-正负例变化，auc变化，ks变化
export async function validityHistogram(params) {
    return request(`/model-monitor/v1/validity/histogram`, {
        method: 'POST',
        body: params,
        postType: 1
    });
}



// 8、模型稳定性报告-折线图
export async function lineChart(params) {
    return request(`/model-monitor/v1/psi/lineChart`, {
        method: 'POST',
        body: params,
        postType: 1
    });
}
// 9、模型稳定性报告-图表
export async function psiReport(params) {
    return request(`/model-monitor/v1/psi/report`, {
        method: 'POST',
        body: params,
        postType: 1
    });
}

// 6、模型有效性监控--图表
export async function predictor(params) {
    return request(`/model-monitor/v1/predictor`, {
        method: 'GET',
        params: params,
        // postType: 1
    });
}

// 6、模型有效性监控--图表
export async function batchPredictor(params) {
    return request(`/model-monitor/v1/batchPredictor`, {
        method: 'GET',
        params: params,
        // postType: 1
    });
}
// 模型有效性监控--图表
export async function validityQuery(params) {
    return request(`/model-monitor/v1/validity/query`, {
        method: 'POST',
        body: params,
        postType: 1
    });
}
// 模型有效性监控--图表
export async function points(params) {
    return request(`/model-monitor/v1/points`, {
        method: 'POST',
        body: params,
        postType: 1
    });
}

// 6、模型有效性监控--图表
export async function modelDetails(params) {
    return request(`/model-monitor/v1/details`, {
        method: 'GET',
        params: params,
        // postType: 1
    });
}
