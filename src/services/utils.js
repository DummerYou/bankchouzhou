import moment from 'moment';
//手机号码
export function isMobile (val) {
    return /^(((13[0-9]{1})|(14[0-9]{1})|(15[0-9]{1})|(16[0-9]{1})|(17[0-9]{1})|(18[0-9]{1})|(19[0-9]{1}))+\d{8})$/.test(val);
};
//手机号验证
export function mobileValidator (rule, value, call) {
    if (!value) call();
    if (isMobile(value)) {
        call();
    } else {
        call(new Error());
    }
}


//姓名验证
export function isName (val) {
    return /^[\u4E00-\u9FA5\uf900-\ufa2d·s]{2,20}$/.test(val);
};
//姓名验证
export function nameValidator (rule, value, call) {
    if (!value) call();
    if (isName(value)) {
        call();
    } else {
        call(new Error());
    }
}


//密码长度至少8位，最多16位，必须包含英文大小写和数字
export function isPassTypeOk (val) {
    // return /^(?![a-z]+$)(?![A-Z]+$)(?!\d+$)[a-zA-Z\d]{8,16}$/.test(val);
    return /^(?=.*[0-9].*)(?=.*[A-Z].*)(?=.*[a-z].*).{8,16}$/.test(val);
};
//密码长度至少8位，最多16位，必须包含英文大小写和数字
export function userPassValidator (rule, value, call) {
    if (!value) call();
    if (isPassTypeOk(value)) {
        call();
    } else {
        call(new Error());
    }
}


function uzStorage () {
    return window.localStorage;
}

export function setStorage (key, value) {
    if (arguments.length === 2) {
        var v = value;
        if (typeof v == 'object') {
            v = JSON.stringify(v);
            v = 'obj-' + v;
        } else {
            v = 'str-' + v;
        }
        var ls = uzStorage();
        if (ls) {
            ls.setItem(key, v);
        }
    }
};

export function getStorage (key) {
    var ls = uzStorage();
    if (ls) {
        var v = ls.getItem(key);
        if (!v) {
            return;
        }
        if (v.indexOf('obj-') === 0) {
            v = v.slice(4);
            return JSON.parse(v);
        } else if (v.indexOf('str-') === 0) {
            return v.slice(4);
        }
    }
};

export function rmStorage (key) {
    var ls = uzStorage();
    if (ls && key) {
        ls.removeItem(key);
    }
};

export function clearStorage () {
    var ls = uzStorage();
    if (ls) {
        ls.clear();
    }
};

//根据 年月日时分秒毫秒的数字 返回标准moment格式
export function getMoment (time) {
    time = time.toString();
    return moment([time.substr(0, 4), Number(time.substr(4, 2)) - 1, time.substr(6, 2), time.substr(8, 2), time.substr(10, 2), time.substr(12, 2), time.substr(14, 3)])
}

export function getCookie (name) {
    var arr, reg = new RegExp("(^| )" + name + "=([^;]*)(;|$)");
    return (arr = document.cookie.match(reg)) ? unescape(arr[2]) : null;
}

//JS操作cookies方法!

//写cookies

export function setCook(name,value)
{
    var Days = 30;
    var exp = new Date();
    exp.setTime(exp.getTime() + Days*24*60*60*1000);
    document.cookie = name + "="+ escape (value) + ";expires=" + exp.toGMTString();
}

//读取cookies
export function getCook(name)
{
    var arr,reg=new RegExp("(^| )"+name+"=([^;]*)(;|$)");
 
    if(arr=document.cookie.match(reg))
 
        return unescape(arr[2]);
    else
        return null;
}

//删除cookies
export function delCook(name)
{
    var exp = new Date();
    exp.setTime(exp.getTime() - 1);
    var cval=getCook(name);
    if(cval!=null)
        document.cookie= name + "="+cval+";expires="+exp.toGMTString();
}